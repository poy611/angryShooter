﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ScoreMessageUI : MonoBehaviour {

	public string canvas_name = "Messages";
	public int w = 1920;
	public int h = 1080;
	public int score;
	public float offset;
	public float offsetDegree;
	public  float offsetDistance = 50;
	public float my_x = 0, my_y = 0;
	Vector3 pos;
	public GameObject messages;
	void Start () {
		
		if(GetComponent<Text> () == null)
			gameObject.AddComponent<Text> ();
		GetComponent<Text> ().text = score.ToString ();

		pos = Camera.main.WorldToViewportPoint (GetComponent<Transform> ().position);
		//transform.SetParent (GameObject.Find(canvas_name).transform, false);

		transform.rotation = Camera.main.transform.rotation;
		GetComponent<RectTransform> ().anchoredPosition = new Vector3(pos.x*w, pos.y*h, 0);

		StartCoroutine (startAnimation ());
	}
	IEnumerator startAnimation(){
		//yield return new WaitForSeconds (1);
		//float my_x = 0, my_y = 0;

		GameObject.Find ("DebugLog").GetComponent<Logger> ().LogText 
		(gameObject.name + ":SM/sA/세팅");
		offset = Mathf.Tan (offsetDegree) * offsetDistance * offset;
		while (my_y < 0.9*offsetDistance) {
			float tmp_x = 0.7f * (offset - my_x), tmp_y = 0.7f*(offsetDistance-my_y);
			GetComponent<RectTransform> ().localPosition = new Vector2 (
				GetComponent<RectTransform> ().localPosition.x + tmp_x,
				GetComponent<RectTransform> ().localPosition.y + tmp_y);
			my_y += tmp_y;
			my_x += tmp_x;
			yield return null;

		}
		Debug.Log ("끄읕");
		float time = 1;
		while (time > Time.deltaTime) {
			time -= Time.deltaTime;
			if(GetComponent<Image> () !=null)
				GetComponent<Text> ().color = new Color(0,0,0,time);
			GetComponent<RectTransform> ().localPosition = new Vector2 (
				GetComponent<RectTransform> ().localPosition.x,
				GetComponent<RectTransform> ().localPosition.y + 3f);
			yield return null;
		}
		Destroy (this.gameObject);
	}
}

