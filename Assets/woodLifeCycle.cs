﻿using UnityEngine;
using System.Collections;

public class woodLifeCycle : blockLifeCycle {
	public float block_hp = 2;

	public override void setup(){
		_blockHP = block_hp;

		blockType = BlockType.WOOD;

	}

	// update전에 수행
	public override void routine_event(){
		
	}
	// On Collision Enter수행 결정
	public override bool collide_event(Collision col){
		
		return false;
	}
	public override void SetTextureByHp (int hp)
	{
		string objName = transform.name;
	
		if (hp < 25) {
			if(objName == "Wood(1x1x1)"){
				Texture txt = Resources.Load ("Textures/block_wood_25%", typeof(Texture)) as Texture;
				GetComponent<Renderer> ().material.EnableKeyword("_DETAIL_MULX2");
				GetComponent<Renderer> ().material.SetTexture("_DetailAlbedoMap", txt);
			}
			else if(objName == "Wood(1x1x2)"){
				Texture txt = Resources.Load ("Textures/block_wood_1x1x2_25%", typeof(Texture)) as Texture;
				GetComponent<Renderer> ().material.EnableKeyword("_DETAIL_MULX2");
				GetComponent<Renderer> ().material.SetTexture("_DetailAlbedoMap", txt);
			}
			else if(objName == "Wood(1x1x3)"){
				Texture txt = Resources.Load ("Textures/block_wood_1x1x3_25%", typeof(Texture)) as Texture;
				GetComponent<Renderer> ().material.EnableKeyword("_DETAIL_MULX2");
				GetComponent<Renderer> ().material.SetTexture("_DetailAlbedoMap", txt);
			}
			else if(objName == "Wood(1x2x2)"){
				Texture txt = Resources.Load ("Textures/block_wood_1x2x2_25%", typeof(Texture)) as Texture;
				GetComponent<Renderer> ().material.EnableKeyword("_DETAIL_MULX2");
				GetComponent<Renderer> ().material.SetTexture("_DetailAlbedoMap", txt);
			}
			else if(objName == "Wood(1x3x3)"){
				Texture txt = Resources.Load ("Textures/block_wood_1x3x3_25%", typeof(Texture)) as Texture;
				GetComponent<Renderer> ().material.EnableKeyword("_DETAIL_MULX2");
				GetComponent<Renderer> ().material.SetTexture("_DetailAlbedoMap", txt);
			}

		} else if (hp < 50) {

			if(objName == "Wood(1x1x1)"){
				Texture txt = Resources.Load ("Textures/block_wood_50%", typeof(Texture)) as Texture;
				GetComponent<Renderer> ().material.EnableKeyword("_DETAIL_MULX2");
				GetComponent<Renderer> ().material.SetTexture("_DetailAlbedoMap", txt);
			}
			else if(objName == "Wood(1x1x2)"){
				Texture txt = Resources.Load ("Textures/block_wood_1x1x2_50%", typeof(Texture)) as Texture;
				GetComponent<Renderer> ().material.EnableKeyword("_DETAIL_MULX2");
				GetComponent<Renderer> ().material.SetTexture("_DetailAlbedoMap", txt);
			}
			else if(objName == "Wood(1x1x3)"){
				Texture txt = Resources.Load ("Textures/block_wood_1x1x3_50%", typeof(Texture)) as Texture;
				GetComponent<Renderer> ().material.EnableKeyword("_DETAIL_MULX2");
				GetComponent<Renderer> ().material.SetTexture("_DetailAlbedoMap", txt);
			}
			else if(objName == "Wood(1x2x2)"){
				Texture txt = Resources.Load ("Textures/block_wood_1x2x2_50%", typeof(Texture)) as Texture;
				GetComponent<Renderer> ().material.EnableKeyword("_DETAIL_MULX2");
				GetComponent<Renderer> ().material.SetTexture("_DetailAlbedoMap", txt);
			}
			else if(objName == "Wood(1x3x3)"){
				Texture txt = Resources.Load ("Textures/block_wood_1x3x3_50%", typeof(Texture)) as Texture;
				GetComponent<Renderer> ().material.EnableKeyword("_DETAIL_MULX2");
				GetComponent<Renderer> ().material.SetTexture("_DetailAlbedoMap", txt);
			}

		} else if (hp < 75) {

			if(objName == "Wood(1x1x1)"){
				Texture txt = Resources.Load ("Textures/block_wood_75%", typeof(Texture)) as Texture;
				GetComponent<Renderer> ().material.EnableKeyword("_DETAIL_MULX2");
				GetComponent<Renderer> ().material.SetTexture("_DetailAlbedoMap", txt);
			}
			else if(objName == "Wood(1x1x2)"){
				Texture txt = Resources.Load ("Textures/block_wood_1x1x2_75%", typeof(Texture)) as Texture;
				GetComponent<Renderer> ().material.EnableKeyword("_DETAIL_MULX2");
				GetComponent<Renderer> ().material.SetTexture("_DetailAlbedoMap", txt);
			}
			else if(objName == "Wood(1x1x3)"){
				Texture txt = Resources.Load ("Textures/block_wood_1x1x3_75%", typeof(Texture)) as Texture;
				GetComponent<Renderer> ().material.EnableKeyword("_DETAIL_MULX2");
				GetComponent<Renderer> ().material.SetTexture("_DetailAlbedoMap", txt);
			}
			else if(objName == "Wood(1x2x2)"){
				Texture txt = Resources.Load ("Textures/block_wood_1x2x2_75%", typeof(Texture)) as Texture;
				GetComponent<Renderer> ().material.EnableKeyword("_DETAIL_MULX2");
				GetComponent<Renderer> ().material.SetTexture("_DetailAlbedoMap", txt);
			}
			else if(objName == "Wood(1x3x3)"){
				Texture txt = Resources.Load ("Textures/block_wood_1x3x3_75%", typeof(Texture)) as Texture;
				GetComponent<Renderer> ().material.EnableKeyword("_DETAIL_MULX2");
				GetComponent<Renderer> ().material.SetTexture("_DetailAlbedoMap", txt);
			}

		}
	}
}
