﻿using UnityEngine;
using System.Collections;
using System;

public class EndInTimer : MonoBehaviour {
	[Header("사용자지정기한(yyyyMMdd)")]
	public string limit = "20170110";
	[Header("오늘")]
	public string NowDate;
	[Header("설정된 플레이제한시간")]
	public string DueDate;

	void Start(){
		System.DateTime d = System.DateTime.Now;
		System.DateTime td = System.DateTime.ParseExact (limit, "yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture);


		NowDate = String.Format ("{0:yyyy MMMMM dd}", d);
		DueDate = String.Format ("{0:yyyy MMMMM dd}", td);

		if (0 < System.DateTime.Compare (d, td))
		{//	Application.Quit ();
		}
	}

}
