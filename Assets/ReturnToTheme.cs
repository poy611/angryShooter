﻿using UnityEngine;
using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;

public class ReturnToTheme : MonoBehaviour {

	public string currentSceneName;
	public string currentThemeName;
	private int past_record = 0;
	public void Move(){
		PlayerPrefs.SetInt ("isPlayedNow", -1);
		if (PlayerPrefs.HasKey (currentThemeName + "_" + currentSceneName))
			past_record = PlayerPrefs.GetInt (currentThemeName + "_" + currentSceneName);
		int current_record = 3;
		if (current_record < past_record)
			current_record = past_record;
		PlayerPrefs.SetInt (currentThemeName + "_" + currentSceneName, current_record);
		UnityEngine.SceneManagement.SceneManager.LoadScene ("Menu_"+currentThemeName);
	}
}
