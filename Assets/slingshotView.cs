﻿using UnityEngine;
using System.Collections;

public class slingshotView : MonoBehaviour {

	public GameObject cam;
	public GameObject observeMode;
	public GameObject shootMode;
	public GameObject slingshot;
	public slingshotStates state;
	[Header("관찰모드로 전환하게 되는 시점입니다")]
	public float ObserveDistance;
	[Header("관찰모드로 전환하게 되는 각도입니다")]
	public float ObserveDegree = 0.05f;
	[Header("현재 카메라와 이미지 마커 사이 거리입니다.")]
	public float distance;
	[Header("옵션에 따른 전환상태를 나타냅니다.")]
	public bool degree_too_low = false;
	void Start(){
		cam = GameObject.Find ("Camera");
		state = GetComponent<slingshotStates> ();
		updateModel ();
	}
	public void updateModel(){
		slingshot = GameObject.Find ("slingshot");
	}
	public void UpdateObserveDist(float dist){
		ObserveDistance = dist;
	}
	void Update(){
		distance = Vector3.Distance (cam.transform.position, GameObject.Find ("ImageTarget").transform.position);
		Vector3 degree = GameObject.Find ("ARCamera").transform.forward;
		degree_too_low = (degree.y < ObserveDegree - 1);

		bool launched = state.state == slingshotStates.launchState.launched;
		bool distance_too_close = distance < ObserveDistance;
		if (degree_too_low || distance_too_close || launched) {
			observeMode.SetActive (true);
			shootMode.SetActive (false);
			slingshot.SetActive (false);
		} else {
			observeMode.SetActive (false);
			shootMode.SetActive (true);
			slingshot.SetActive (true);
		}
		degree_too_low = false;
	}

}
