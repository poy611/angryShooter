﻿using UnityEngine;
using System.Collections;
using System.Reflection.Emit;

public class SetActiveWithObject : MonoBehaviour {

	public string objName;
	public GameObject obj;
	void Update () {

		if (GameObject.Find (objName) == null)
			obj.SetActive (false);
		else
			obj.SetActive (true);
	}
}
