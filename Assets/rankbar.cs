﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class rankbar : MonoBehaviour {
	public int  rank;
	public string myname;
	public GameObject mypos;
	public GameObject ranktrophy;
	public GameObject ranknum1;
	public GameObject ranknum2;
	public GameObject rankchar;
	public Text rankcharname;
	public Text rankcharscore;

	void Start(){
		myname = GameObject.Find ("MyName").GetComponent<Text> ().text;
		switch (rank) {
		case 1: 
			ranktrophy.GetComponent<Image> ().sprite = (Sprite)Resources.Load ("UI/pu_rk_trophy_01", typeof(Sprite));
			break;
		case 2:
			ranktrophy.GetComponent<Image> ().sprite = (Sprite)Resources.Load ("UI/pu_rk_trophy_02", typeof(Sprite));
			break;
		case 3:
			ranktrophy.GetComponent<Image> ().sprite = (Sprite)Resources.Load ("UI/pu_rk_trophy_03", typeof(Sprite));
			break;
		case 4: case 5: case 6: case 7: case 8: case 9:
			ranknum1.GetComponent<Image> ().sprite = (Sprite)Resources.Load ("UI/pu_pt_number_" + rank.ToString (), typeof(Sprite));
			ranknum1.SetActive (true);
			ranktrophy.SetActive (false);
			break;
		default:
			int fd = rank / 10, sd = rank % 10;
			ranknum1.GetComponent<Image> ().sprite = (Sprite)Resources.Load ("UI/pu_pt_number_" + fd.ToString (), typeof(Sprite));
			ranknum2.GetComponent<Image> ().sprite = (Sprite)Resources.Load ("UI/pu_pt_number_" + sd.ToString (), typeof(Sprite));
			ranknum1.GetComponent<RectTransform> ().offsetMin = new Vector2 (72f, -53f);
			ranknum1.GetComponent<RectTransform> ().SetSizeWithCurrentAnchors (RectTransform.Axis.Horizontal, 67);
			ranknum1.GetComponent<RectTransform> ().SetSizeWithCurrentAnchors (RectTransform.Axis.Vertical, 84);
			ranknum2.GetComponent<RectTransform> ().offsetMin = new Vector2(127f, -53f);
			ranknum2.GetComponent<RectTransform> ().SetSizeWithCurrentAnchors (RectTransform.Axis.Horizontal, 67);
			ranknum2.GetComponent<RectTransform> ().SetSizeWithCurrentAnchors (RectTransform.Axis.Vertical, 84);
			ranktrophy.SetActive (false);
			ranknum1.SetActive (true);
			ranknum2.SetActive (true);
			break;
			
		}
		if (rankcharname.text == myname) {
			mypos.SetActive (true);
			GameObject.Find ("RankMyRankBtn").GetComponent<OnMyRankBtn> ().MyRank = transform.gameObject;
		}else
			mypos.SetActive (false);
	}
}
