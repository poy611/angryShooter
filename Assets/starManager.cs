﻿using UnityEngine;
using System.Collections;

public class starManager : MonoBehaviour {

	public int stars = 0;
	public GameObject star1;
	public GameObject star2;
	public GameObject star3;
	public string currentTheme;

	public void setStar(int sceneNum){
		string tempTheme=UnityEngine.SceneManagement.SceneManager.GetActiveScene ().name;
		currentTheme = tempTheme.Substring(5,tempTheme.Length-5);
		if (!PlayerPrefs.HasKey (currentTheme + "_Scene" + sceneNum + "_StarCount")) {
			PlayerPrefs.SetInt (currentTheme + "_Scene" + sceneNum + "_StarCount", 0);
		}
		Debug.Log (currentTheme);
		stars = PlayerPrefs.GetInt (currentTheme + "_Scene" + sceneNum + "_StarCount");
		switch (stars) {
		case 3:
			star3.SetActive (false);
			star2.SetActive (false);
			star1.SetActive (false);
			break;
		case 2:
			star2.SetActive (false);
			star1.SetActive (false);
			break;
		case 1:
			star1.SetActive (false);
			break;
		default:
			break;
		}
	}
	public int getStar(){
		return stars;
	}
}
