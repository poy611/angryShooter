﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
public class switchUI : MonoBehaviour {

	public GameObject enabledObj;
	public GameObject disabledObj;
	bool newImageCo=false;
	public void enableUI(){
		if (enabledObj != null)
			enabledObj.SetActive (true);
		if (disabledObj != null)
			disabledObj.SetActive (false);

		//StartCoroutine (EnableUIEffect ());
	}

	public void disableUI(){
		SoundManage.instance.buttonClickPlay ();
		if(enabledObj != null)
			enabledObj.SetActive (false);
		if(disabledObj != null)
			disabledObj.SetActive (true);
	}
	public void toggleUI(){
		if(enabledObj != null)
			enabledObj.SetActive (!enabledObj.activeInHierarchy);
		if(disabledObj != null)
			disabledObj.SetActive (!disabledObj.activeInHierarchy);
	}
	public void enableUIEffect(){
		StartCoroutine (EnableUIEffect ());
	}
	public void Quit(){
		SoundManage.instance.buttonClickPlay ();
		Application.Quit ();
	}
	public void NewImageEnable(){
		this.gameObject.SetActive (true);
		if (!newImageCo) {
			StartCoroutine (NewImageEffect ());
		}
	}
	public void StopCoNewImage(){
		StopAllCoroutines ();
		newImageCo = false;
	}
	IEnumerator NewImageEffect(){
		newImageCo = true;
		while (true) {
			while (true) {
				if (this.transform.localScale.x >= 1.2f && this.transform.localScale.y >= 1.2f) {
					break;
				}
				this.transform.localScale += new Vector3 (Time.deltaTime*0.5f, Time.deltaTime*0.5f, 0f);

				yield return null;
			}
			while (true) {

				if (this.transform.localScale.x <= 1.0f && this.transform.localScale.y <= 1.0f) {
					break;
				}
				this.transform.localScale -= new Vector3 (Time.deltaTime*0.5f, Time.deltaTime*0.5f, 0f);

				yield return null;
			}
		
			yield return null;
		}
	}
	IEnumerator EnableUIEffect(){
		GameObject Aura;
		Image disableImg = disabledObj.GetComponent<Image> ();

		Aura = enabledObj.transform.FindChild ("Aura").gameObject;
		GameObject.Find (transform.name.Substring (0, transform.name.Length - 1) + "" + (Convert.ToInt32 (transform.name [transform.name.Length - 1].ToString ()) - 1)).transform.FindChild ("Enabled").transform.FindChild ("Line").gameObject.SetActive (true);
		Image Line = GameObject.Find (transform.name.Substring(0,transform.name.Length-1)+""+(Convert.ToInt32(transform.name[transform.name.Length-1].ToString())-1)).transform.FindChild("Enabled").transform.FindChild("Line").gameObject.GetComponent<Image>();
		Line.fillAmount = 0f;
		while (true) {
			if (Line.fillAmount >= 1.0f) {
				break;
			}
			Line.fillAmount += Time.deltaTime;
			yield return null;
		}
		Aura.transform.localScale = new Vector3 (1.2f, 1.2f, 1f);
		enabledObj.SetActive (true);
		Color color = disableImg.color;
		while (true) {
			
			if (Aura.transform.localScale.x <= 1.0f && Aura.transform.localScale.y <= 1.0f) {
				break;
			}
			color.a -= Time.deltaTime*0.5f;
			disableImg.color = color;
			Debug.Log (color.a);
			Aura.transform.localScale -= new Vector3 (Time.deltaTime*0.1f, Time.deltaTime*0.1f, 0f);
	
			yield return null;
		}
		disabledObj.SetActive (false);

	
		for (int i = 2; i <= 5; i++) {
			PlayerPrefs.SetInt ("NextScene"+i,0);
		}

	}
}
