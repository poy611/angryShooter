﻿using UnityEngine;
using System.Collections;

public class drawerLauncher : MonoBehaviour {

	public GameObject flyingBall;
	public AudioClip shoot;
	public bool isLaunched = false;
	[Header("발사파워는 defaultPower * shoot_power입니다")]
	public float defaultPower = 5.0f;
	[Header("발사한 공의 이름입니다")]
	public string defaultBall = "Ball(stone)";
	[Header("9에서 9.9사이로만 조정하세요")]
	public float shootDegree = 20.0f;
	gameplay _gameplay;
	void Start () {
		_gameplay = GameObject.Find ("System").GetComponent<gameplay> ();
	}

	public void launchBall(float power){
		
		SoundManage.instance.SlingPlay ();
		float shoot_power = GetComponent<drawerDraggable> ().shoot_power;
		if (shoot_power < 0)
			return;
		/*
		string model = GetComponent<drawerLoader> ().unloadBall ();
		if (model == null)
		return;
		
		// 로드

		flyingBall = Instantiate (Resources.Load ("Balls/Ball(" + model + ")"), transform.position, transform.rotation) as GameObject;
		flyingBall.GetComponent<Transform> ().parent = this.gameObject.transform;
		flyingBall.GetComponent<Transform> ().localPosition = 
			new Vector3(0,0,GetComponent<drawerLoader> ().posOffset);
		flyingBall.name = model;
		flyingBall.GetComponent<Rigidbody> ().useGravity = true;
*/
		if (GameObject.FindWithTag ("ball") == null)
			return;
		
		flyingBall = GameObject.FindWithTag ("ball");
		string model = flyingBall.name;
	
		flyingBall.GetComponent<Transform> ().parent = this.gameObject.transform;
		flyingBall.GetComponent<Transform> ().localPosition = 
			new Vector3(0,0,GetComponent<drawerLoader> ().posOffset);
		flyingBall.name = model;
		flyingBall.GetComponent<Rigidbody> ().useGravity = true;
		flyingBall.GetComponent<SphereCollider> ().isTrigger = false;
		// 방향 설정.
		flyingBall.GetComponent<Rigidbody> ().AddForce (
			transform.forward * defaultPower * shoot_power);
		// degree
		flyingBall.GetComponent<Rigidbody> ().AddForce (
			transform.up * defaultPower * shoot_power * Mathf.Tan (shootDegree));
		
		GameObject IM = GameObject.Find ("ImageTarget");
		if (IM == null)
			flyingBall.transform.parent = null;
		else
			flyingBall.transform.parent = IM.transform;
		
		_gameplay.BallSelect (model);
//		flyingBall.AddComponent<> ();
//		 속도 결정.
	}
}
