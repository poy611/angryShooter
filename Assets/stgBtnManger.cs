﻿using UnityEngine;
using System.Collections;

public class stgBtnManger : MonoBehaviour {

	// Use this for initialization
	public int currentStage;
	public string currentTheme;
	public float jumpheight = 2.0f;
	public float x;
	public float u;
	public float f;
	public float d;
	public float basicheight;
	public GameObject character;
	public GameObject ch;
	public GameObject finished;
	public GameObject unlocked;

	void Start () {
		int j;
		if (!PlayerPrefs.HasKey ("TotalScore")) {
			PlayerPrefs.SetInt ("TotalScore",0);
		}
		for (j = 1; j<11; j++) {


			if (PlayerPrefs.HasKey (currentTheme + "_Scene" + j.ToString ())) {
				


				if (PlayerPrefs.GetInt (currentTheme + "_Scene" + j.ToString ()) > 0) {
					
					GameObject.Find ("Stage" + j.ToString () + "/Stars").GetComponent<starManager> ().setStar (j);
					currentStage = j+ 1;
				}

			}
			else {
				
			

				GameObject.Find ("Stage" + j.ToString () + "/Stars").GetComponent<starManager> ().setStar (j);
				currentStage = j + 1;
				break;
			}
				
		}
		Debug.Log ("currentStage : " + currentStage);
		string stg = "Stage" + (currentStage-2).ToString ();
		unlocked = GameObject.Find (stg);
		finished = GameObject.Find ("Stage" + (currentStage - 1).ToString ());
		character = GameObject.Find ("Player").transform.FindChild(PlayerPrefs.GetString ("Character")).gameObject;
		//ch = GameObject.Find ("Character");
		//ch=GameObject.Find("berry");
		GameObject stgDeActivated = GameObject.Find (stg + "/DeActivated");
		GameObject stgLock = GameObject.Find (stg + "/DeActivated/Lock");

		switch (currentStage) {
		case 0: 
		case 1:
			break;
		
		case 2:
			for (int i = 2; i < currentStage; i++) {
				string tmp = "Stage" + i.ToString ();

				GameObject.Find (tmp + "/DeActivated").SetActive (false);
			}
			character.transform.localPosition = finished.transform.localPosition+new Vector3(99f,-97f,0f);
			break;
		default:
			for (int i = 2; i < currentStage; i++) {
				string tmp = "Stage" + i.ToString ();

				GameObject.Find (tmp + "/DeActivated").SetActive (false);
			}

			if (currentStage >= 8) {
				if (UnityEngine.SceneManagement.SceneManager.GetActiveScene ().name == "Menu_Theme4") {
					RectTransform rectTransform = GameObject.Find ("Content").GetComponent<RectTransform> ();
					rectTransform.offsetMin = new Vector2 (rectTransform.offsetMin.x, -175f);
					rectTransform.offsetMax = new Vector2 (rectTransform.offsetMax.x, 365f);
				} else {
					RectTransform rectTransform = GameObject.Find ("Content").GetComponent<RectTransform> ();
					rectTransform.offsetMin = new Vector2 (rectTransform.offsetMin.x, -540f);
					rectTransform.offsetMax = new Vector2 (rectTransform.offsetMax.x, -540f);
				}
			}
			if (PlayerPrefs.GetInt ("CharacterEffect") == 1 && PlayerPrefs.GetInt (UnityEngine.SceneManagement.SceneManager.GetActiveScene ().name+currentStage)==0) {
				StartCoroutine (moveCharacter ());
			} else {
				PlayerPrefs.SetInt ("CharacterEffect", 0);
				character.transform.localPosition = finished.transform.localPosition+new Vector3(99f,-97f,0f);

			}
			//Destroy (stgDeActivated);
			break;
		}
	
	}
	public void startStage(int stgnum){
		SoundManage.instance.buttonClickPlay ();
		PlayerPrefs.SetInt (UnityEngine.SceneManagement.SceneManager.GetActiveScene ().name+currentStage,1);
		UnityEngine.SceneManagement.SceneManager.LoadScene(
				currentTheme + "_" + "Scene" + stgnum);
	}
	IEnumerator moveCharacter(){
		PlayerPrefs.SetInt ("CharacterEffect", 0);
		character.transform.localPosition = unlocked.transform.localPosition+new Vector3(99f,-97f,0f);
		while (character.transform.localScale.y >= 3f) {
			character.transform.localScale -= new Vector3 (60f*Time.deltaTime, 60f*Time.deltaTime, 0);
			character.transform.localPosition -= new Vector3 (-5f*Time.deltaTime, 110f*Time.deltaTime, 0);
			yield return null;
		}

		character.transform.localPosition = finished.transform.localPosition+new Vector3(99f,-200f,0f);
		while (character.transform.localScale.y <= 60f) {
			character.transform.localScale += new Vector3 (60f*Time.deltaTime, 60f*Time.deltaTime, 0);
			character.transform.localPosition += new Vector3 (0, 105f*Time.deltaTime, 0);
			yield return null;
		}
		character.transform.localPosition = finished.transform.localPosition+new Vector3(99f,-97f,0f);
		yield return null;

	}
}
