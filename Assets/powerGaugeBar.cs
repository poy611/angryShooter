﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class powerGaugeBar : MonoBehaviour {


	public float full_power;
	public Image gauge;
	public drawerDraggable drag;

	void Start(){
		gauge = GameObject.Find ("Gauge").GetComponent<Image> ();
		//if (GameObject.Find ("drawer") == null)
		//	drag = GameObject.Find ("drawer").GetComponent<drawerDraggable> ();
	}
	void Update(){
		if (GameObject.Find ("drawer") != null) {
			if (drag == null)
				drag = GameObject.Find ("drawer").GetComponent<drawerDraggable> ();
			full_power = drag.power_ratio;
			gauge.fillAmount = full_power;
		}
	}
}
