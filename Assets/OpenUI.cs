﻿using UnityEngine;
using System.Collections;

public class OpenUI : switchUI {

	public GameObject openObj;
	public void CustomEnableUI(){
		openObj.SetActive (true);
		enableUI ();
	}
	public void CustomDisableUI(){
		openObj.SetActive (false);
		disableUI ();
	}
	public void CustomToggleUI(){
		openObj.SetActive (!openObj.activeSelf);
		toggleUI ();
	}
}
