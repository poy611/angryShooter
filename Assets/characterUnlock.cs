﻿using UnityEngine;
using System.Collections;

public class characterUnlock : MonoBehaviour {
	public int theme;
	void Start(){
		theme = (GameObject.Find ("Themes").GetComponent<ThemeMgr> ().currentTheme) - 1;
		if (theme >= 2)
			GameObject.Find ("CharacterFH2").GetComponent<switchUI> ().enableUI ();
		if (theme >= 3)
			GameObject.Find ("CharacterFH3").GetComponent<switchUI> ().enableUI ();
		if (theme >= 4)
			GameObject.Find ("CharacterFH4").GetComponent<switchUI> ().enableUI ();
	}
}
