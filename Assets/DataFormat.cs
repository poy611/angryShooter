﻿using UnityEngine;
using System;

[Serializable]
class Save{
	public string Time;
	public string Score;
	public string Stars;
}
