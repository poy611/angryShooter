﻿using UnityEngine;
using System.Collections;

public class shooter : MonoBehaviour {

	public float interval;
	public float power = 200.0f;
	public GameObject slingshot;
	void Start(){
		StartCoroutine(shoot());
	}
	void Update(){
		slingshot = GameObject.Find ("slingshot");
	}
	IEnumerator shoot(){
		while (true) {
			if (slingshot != null) {
				GameObject bullet = Instantiate (Resources.Load ("Enemy/bullet")) as GameObject;
				bullet.GetComponent<Transform> ().position = transform.position;
				bullet.GetComponent<Transform> ().LookAt (slingshot.transform.position);
				bullet.GetComponent<Rigidbody> ().AddForce (bullet.transform.forward * power);
			}
			yield return new WaitForSeconds (interval);
		}
	}
}
