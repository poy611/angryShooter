﻿using UnityEngine;
using System.Collections;

public class ballBomb : MonoBehaviour {

	public float bombPower = 3.0f;
	public bool isDone = false;
	public float curSize;
	public GameObject explosion;

	void OnCollisionEnter(Collision col){
		Debug.Log (col);
		if (col.gameObject.tag != "straw") {
			if (!isDone) {
				if (transform.name == "bomb") {
					explosion = Instantiate (Resources.Load ("Effect/explosion_stylized_small_originalFire")) as GameObject;
					explosion.GetComponent<Transform> ().position = GetComponent<Transform> ().position;
					SphereCollider myCollider = transform.GetComponent<SphereCollider> ();
					curSize = myCollider.radius;
					myCollider.radius = curSize * bombPower;
					Invoke ("resizeCollider", 1f);
				} else if (transform.name == "strait") {
					explosion = Instantiate (Resources.Load ("Effect/explosion_stylized_small_demonFire")) as GameObject;
					explosion.GetComponent<Transform> ().position = GetComponent<Transform> ().position;
				} else {
					explosion = Instantiate (Resources.Load ("Effect/hit-fire")) as GameObject;
					explosion.GetComponent<Transform> ().position = GetComponent<Transform> ().position;
				}

			}
		}
		transform.GetComponent<ballLifeCycle> ().deleteBall ();
	}
	void resizeCollider(){
		SphereCollider myCollider = transform.GetComponent<SphereCollider> ();
		myCollider.radius = curSize;
		Destroy (this.gameObject);
	}
}
