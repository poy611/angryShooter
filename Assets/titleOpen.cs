﻿using UnityEngine;
using System.Collections;

public class titleOpen : MonoBehaviour {
	public GameObject titleWindow;

	static bool game_on = true;

	void Start () {
		if (game_on) {
			game_on = false;
			titleWindow.SetActive (true);
			SoundManage.instance.BgmTitlePlay ();
		}
	}
}
