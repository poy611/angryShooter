﻿using UnityEngine;
using System.Collections;

public class CautionWindowControl : MonoBehaviour {
	public GameObject sslingshot;
	public GameObject iinterface;
	public GameObject Einterface;
	public GameObject MessageInterface;
	public bool onPlay = true;


	public bool cautionActived = false;
	bool isEnd = false;

	void Update(){
		if (onPlay) {
			if (GameObject.Find ("GROUND").GetComponent<Renderer> ().enabled) {
				/* PlayLogger */
				if (!cautionActived) {

					if (GameObject.Find ("DebugLog") != null)
						GameObject.Find ("DebugLog").GetComponent<Logger> ().LogText 
						(gameObject.name + ":CWC/Update()/Ground가Enabled");

					if (GameObject.Find ("DebugLog") != null)
						GameObject.Find ("DebugLog").GetComponent<Logger> ().LogText 
						(gameObject.name + ":CWC/Update()/ErrorWindow띄움");
					cautionActived = true;
				}

				iinterface.SetActive (true);
				sslingshot.SetActive (true);
				MessageInterface.SetActive (true);
				Einterface.SetActive (false);
			} else {
				/* PlayLogger */
				if (cautionActived) {

					if (GameObject.Find ("DebugLog").GetComponent<Logger> ())
						GameObject.Find ("DebugLog").GetComponent<Logger> ().LogText 
						(gameObject.name + ":CWC/Update()/Ground가Disabled");

					if (GameObject.Find ("DebugLog").GetComponent<Logger> ())
						GameObject.Find ("DebugLog").GetComponent<Logger> ().LogText 
						(gameObject.name + ":CWC/Update()/ErrorWindow닫음");
					cautionActived = false;
				}
				iinterface.SetActive (false);
				sslingshot.SetActive (false);
				MessageInterface.SetActive (false);
				Einterface.SetActive (true);
			}
		} else {
			if (!isEnd) {
				isEnd = true;
				//GameObject.Find ("objs").gameObject.SetActive (false);
				iinterface.SetActive (true);
				sslingshot.SetActive (false);
				MessageInterface.SetActive (true);
				Einterface.SetActive (false);
				//iinterface.transform.FindChild ("Window(end)").GetComponent<Result> ().resultWindow ();
			}
		}
	}
}
