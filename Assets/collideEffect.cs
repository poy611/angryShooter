﻿using UnityEngine;
using System.Collections;

public class collideEffect : MonoBehaviour {

	public GameObject go;
	public Vector3 rot;
	public AudioClip crash;

	bool gameStarted = false;
	bool onGround = false;
	void Update(){
		if (gameStarted == false){
			GameObject slingshot = GameObject.Find ("slingshotWrapper");
			if(slingshot != null && slingshot.GetComponent<slingshotStates> ().state == slingshotStates.launchState.launched){
				
				if(GetComponent<blockLifeCycle> () != null)
					GetComponent<blockLifeCycle> ().resetBlockHp ();
				if(GetComponent<caseMgr> () != null)
					GetComponent<caseMgr> ().resetBlockHp ();
				gameStarted = true;
			}
		}
	}
	void OnCollisionStay(Collision col){
		if (col.gameObject.name == "GROUND")
			onGround = true;
		else
			onGround = false;
	}

	void OnCollisionEnter(Collision col){
		go = col.gameObject;
		if (gameStarted) {
			/*if (GetComponent<AudioSource> () == null)
				gameObject.AddComponent<AudioSource> ();
			GetComponent<AudioSource> ().clip = crash;
			GetComponent<AudioSource> ().Play (0);
			*/
	
			if (onGround) {
				foreach (ContactPoint contact in col.contacts) {
					GameObject smoketrail = Resources.Load ("Effect/SmokeTrail_Block") as GameObject;
					Instantiate (smoketrail, contact.point, Quaternion.Euler (contact.normal.x, contact.point.y, contact.point.z));
					Debug.DrawRay (contact.point, contact.normal, Color.white);
				}
			}
		}
	}
}
