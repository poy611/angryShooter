﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;
public class ImageEffectSuccessiveChange : MonoBehaviour {
	public Sprite[] images;
	public float interval;
	public bool smooth;

	private int index = 0;
	private int cur = 0;
	private bool animationOn = false;
	public float speed = 1.5f;
	void Start() {
		index = images.Length;
		if (GetComponent<Image> () == null)
			gameObject.AddComponent<Image> ();
	}
	void Update () {
		if (!animationOn) {
			animationOn = true;
			StartCoroutine (changingAnimation ());
		}
	}

	public void SetImageWithIndex(int num){
		
		Debug.Log ("SetImageWithIndex " + num);
		GetComponent<Image> ().sprite = images [num - 1];
	}

	IEnumerator changingAnimation(){
		cur = 0;

		while (cur < index) {
			GetComponent<Image> ().sprite = null;
			GetComponent<Image> ().sprite = images[cur];
			string sceneName = SceneManager.GetActiveScene ().name;
			if (sceneName == "Menu_Main" || sceneName == "Menu_Theme3") {
				Color color = GetComponent<Image> ().color;
				//blink time
				while (true) {
					yield return null;
					color.a += Time.deltaTime * speed;
					GetComponent<Image> ().color = color;

					if (color.a >= 1) {
						break;
					}
				}
				while (true) {

					yield return null;
					color.a -= Time.deltaTime * speed;
					GetComponent<Image> ().color = color; 
					if (color.a <= 0) {
						break;
					}
				}


			}
				yield return new WaitForSeconds (0.5f);


			cur++;
		}
		animationOn = false;
		cur = 0;
	}

	IEnumerator smoothChangingAnimation(){ 
		while (cur < index) {
			GetComponent<Image> ().sprite = images[cur];
			float time = 0;
			while (time < interval) {
				time += Time.deltaTime;
				yield return null;
			}
			cur++;
		}
		animationOn = false;
		cur = 0;
	}
}
