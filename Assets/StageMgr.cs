﻿using UnityEngine;
using UnityEngine.UI;

public class StageMgr : MonoBehaviour {
	public int current;
	public Text stageText;
	public GameObject stageSlider;
	void Start(){
		
		stageSlider.GetComponent<Slider> ().value = 
			GameObject.Find ("Themes").GetComponent<ThemeMgr> ().clearedPlusOne - 1;
		updateStageText ();
		updateStage ();
	}
	public void updateStageText(){
		current = (int)stageSlider.GetComponent<Slider> ().value + 1;
		PlayerPrefs.SetInt ("currentStage", current - 1);
		if (current == 41) {
			stageText.text = "Last";
		} else {
			int theme = (current-1) / 10 + 1;
			int stage = (current-1) % 10 + 1;
			stageText.text = "Theme" + theme.ToString () + " Stage" + stage.ToString ();
		}
	}
	public void updateStage(){
		for (int i = 1; i <= 40; i++) {
			string stageName;
				int theme = (i - 1) / 10 + 1;
				int stage = (i - 1) % 10 + 1;
				stageName = "Theme" + theme.ToString () + "_Scene" + stage.ToString ();
			Debug.Log (stageName);
			if (i < current)
				PlayerPrefs.SetInt (stageName, 3);
			else {
				PlayerPrefs.SetInt (stageName, -1);
			}
		}

		GameObject.Find ("Themes").GetComponent<ThemeMgr> ().getState ();
	}
}
