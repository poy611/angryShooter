﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class messageUI : MonoBehaviour {

	public string ui_path;
	public string canvas_name;
	public int w;
	public int h;
	GameObject messageImage;
	Vector3 pos;

	void Start () {
		GameObject ui = Resources.Load (ui_path) as GameObject;
		pos = Camera.main.WorldToViewportPoint (GetComponent<Transform> ().position);
		messageImage = Instantiate(ui, pos, Quaternion.identity) as GameObject;
		messageImage.transform.SetParent (GameObject.Find(canvas_name).transform, false);
		messageImage.GetComponent<RectTransform> ().anchoredPosition = new Vector3(pos.x*w, pos.y*h, 0);
		//StartCoroutine (startAnimation ());
		StartCoroutine (autoDestruction ());
	}
	IEnumerator startAnimation(){
		//yield return new WaitForSeconds (2);
		float time = 1;
		while (time > Time.deltaTime) {
			time -= Time.deltaTime;
			if(GetComponent<Image> () !=null)
				messageImage.GetComponent<Image> ().color = new Color(255,255,255,time);
			messageImage.GetComponent<RectTransform> ().localPosition = new Vector2 (
				messageImage.GetComponent<RectTransform> ().localPosition.x,
				messageImage.GetComponent<RectTransform> ().localPosition.y + 3f);
			yield return null;
		}
		//Destroy (this.gameObject);
	}
	IEnumerator autoDestruction(){
		//yield return new WaitForSeconds (2);
		float time = 1;
		while (time > Time.deltaTime) {
			time -= Time.deltaTime;
			if(GetComponent<Image> () !=null)
				messageImage.GetComponent<Image> ().color = new Color(255,255,255,time);
			yield return null;
		}
		Destroy (messageImage);
		//Destroy (this.gameObject);
	}
}
