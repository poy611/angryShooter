﻿using UnityEngine;
using System.Collections;

public class ballLifeCycle : MonoBehaviour {

	public GameObject devicePos;
	public slingshotStates state;
	public float currentDistance;
	public float currentSpeed;
	public float minimumDistance = 0.5f;
	[Header("기본 Ground의 한변의 길이가 5입니다.")]
	public float maximumDistance = 2.5f;
	[Header("실제 앱에 옮겼을때는 0.5가 적절합니다")]
	public float minimumSpeed = 5.0f;

	public Vector3 direction;
	public GameObject ImageTarget;
	public float angle;
	private Vector3 lastPos;
	void Start () {
		devicePos = GameObject.Find ("Camera");
		state = GameObject.Find ("slingshotWrapper").GetComponent<slingshotStates> ();
		ImageTarget = GameObject.Find ("ImageTarget");
		lastPos = transform.position;
	}

	void Update () {
		if (state.state == slingshotStates.launchState.launched) {
			
			currentSpeed = Vector3.Magnitude (GetComponent<Rigidbody> ().velocity);
			currentDistance = Vector3.Distance (gameObject.transform.position, ImageTarget.transform.position);
			Vector3 ImageTargetDir = ImageTarget.transform.position - transform.position;
			direction = transform.position - lastPos;
			angle = Vector3.Dot (ImageTargetDir, direction);
			if (Vector3.Dot (ImageTargetDir, direction)<0){
				
				if(currentDistance > maximumDistance)
					deleteBall ();

				if (currentSpeed < minimumSpeed)
					deleteBall ();
			}
		}
		lastPos = transform.position;
	}

	public void deleteBall(){
		Destroy (gameObject);
	}
}
