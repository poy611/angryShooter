﻿using UnityEngine;
using System.Collections;

public class setCharacterGesture : MonoBehaviour {

	public Transform pos;
	// Use this for initialization


	public void setCharacter(int star){
		string character = PlayerPrefs.GetString ("Character");
		Debug.Log (character);
		GameObject go = Resources.Load ("Character(result)/image(" + character + star.ToString () + ")") as GameObject;
		GameObject image = Instantiate (go, this.transform.position, this.transform.rotation) as GameObject;
		image.transform.parent = pos;
		image.GetComponent<RectTransform> ().localScale = new Vector3 (1,1,1);
		image.GetComponent<RectTransform> ().offsetMin = new Vector2 (0,0);
		image.GetComponent<RectTransform> ().SetSizeWithCurrentAnchors (RectTransform.Axis.Horizontal, 391);
		image.GetComponent<RectTransform> ().SetSizeWithCurrentAnchors (RectTransform.Axis.Vertical, 395);
	}
}
