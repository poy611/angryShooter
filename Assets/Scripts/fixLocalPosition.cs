﻿using UnityEngine;
using System.Collections;

public class fixLocalPosition : MonoBehaviour {
	public bool useOptions;
	public bool fix_x;
	public bool fix_y;
	public bool fix_z;

	public Vector3 debugW;
	private Vector3 originPosition;
	void Start(){
		updatePosition (transform.localPosition);
	}
	void Update(){
		debugW = transform.position;
		Vector3 currentPosition = transform.localPosition;
		if (useOptions) {
			if (fix_x)
				currentPosition.x = originPosition.x;
			else
				updateXPosition ();
			if (fix_y)
				currentPosition.y = originPosition.y;
			else
				updateYPosition ();
			if (fix_z)
				currentPosition.z = originPosition.z;
			else
				updateZPosition ();
		} else
			currentPosition = originPosition;
		
		transform.localPosition = currentPosition;
	}
	public void updatePosition(Vector3 newPosition){
		originPosition = newPosition;
	}
	void updateXPosition(){
		originPosition.x = transform.localPosition.x;
	}
	void updateYPosition(){
		originPosition.y = transform.localPosition.z;
	}
	void updateZPosition(){
		originPosition.z = transform.localPosition.z;
	}
}
