﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class MoveTitleTextEffect : MonoBehaviour {
	public Transform pos1;
	public Transform pos2;
	void OnEnable(){
		StartCoroutine (MoveEffect());
	}
	void OnDisable(){
		StopAllCoroutines ();
	}
	IEnumerator MoveEffect(){
		while (true) {
			this.transform.GetComponent<Image>().enabled=true;
			while (true) {
				this.transform.position = Vector3.MoveTowards (this.transform.position, pos1.position, 8.0f * Time.deltaTime); 

				if (Vector3.Distance (this.transform.position, pos1.position) < 0.01f) {
					break;
				}
				yield return null;
			}
			this.transform.GetComponent<Image>().enabled=false;
			this.transform.position = pos2.position;
			yield return new WaitForSeconds (3.0f);
		}

	}
}
