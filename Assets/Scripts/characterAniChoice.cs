﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class characterAniChoice : MonoBehaviour {

	public GameObject[] characters;
	void Start () {
		
		if (!PlayerPrefs.HasKey ("Character"))
			PlayerPrefs.SetString ("Character", "berry");
		string character = PlayerPrefs.GetString ("Character");
		for (int i = 0; i < characters.Length; i++) {
			if (characters [i].transform.name == character) {
				characters [i].SetActive (true);
				break;
			}
		}
	}
	

}
