﻿using UnityEngine;
using System.Collections;

public class GetBlockType : MonoBehaviour {
	public GameObject _obj;
	public gameplay _gamePlay;
	public blockLifeCycle[] _objs;
	public int sceneTotalLimitScore=0;
	public int waponTotalScore=0;
	// Use this for initialization
	void Start () {
		StartCoroutine (TotalLimitScoreCount ());
	}
	IEnumerator TotalLimitScoreCount(){
		yield return new WaitForSeconds (3.0f);
		sceneTotalLimitScore = 0;
		_obj = GameObject.Find ("ImageTarget");
		_objs = _obj.GetComponentsInChildren<blockLifeCycle> ();
		for (int i = 0; i < _objs.Length; i++) {
			switch (_objs [i].blockType) {

			case blockLifeCycle.BlockType.STONE:
				sceneTotalLimitScore += 100;
				break;
			case blockLifeCycle.BlockType.STRAW:
				sceneTotalLimitScore += 100;
				break;
			case blockLifeCycle.BlockType.WOOD:
				sceneTotalLimitScore += 100;
				break;
			default:
				sceneTotalLimitScore += 0;
				break;
			}
		}
		_gamePlay = GameObject.Find ("System").GetComponent<gameplay> ();
		waponTotalScore += _gamePlay.currentStoneCount * 100*5;
		waponTotalScore += _gamePlay.currentBombCount * 100*5;
		waponTotalScore += _gamePlay.currentStraitCount * 100*5;

	}

}
