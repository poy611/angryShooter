﻿using UnityEngine;
using System.Collections;

public class slingshotAPIs : MonoBehaviour {
	public drawerDraggable d_draggable;
	public drawerElasticity d_elasticity;
	public drawerLauncher d_launcher;
	public drawerLoader d_loader;

	public void updateModel(){
		GameObject drawer = GameObject.Find ("drawer");
		d_draggable = drawer.GetComponent<drawerDraggable> ();
		d_elasticity = drawer.GetComponent<drawerElasticity> ();
		d_launcher = drawer.GetComponent<drawerLauncher> ();
		d_loader = drawer.GetComponent<drawerLoader> ();
	}
	public void loadBall(string model){
		d_loader.loadBall (model);
	}
	public void unloadBall(){
		d_loader.unloadBall ();
	}
	public void shootBall(float power){
		d_launcher.launchBall (power);
	}
}
