﻿using UnityEngine;
using System.Collections;

public class slingshotMgr : MonoBehaviour {

	private GameObject currentModel;
	public Vector3 offset = new Vector3(0,0,0.6f);
	void Start(){
		loadSlingShot ("slingshot_default");
	}

	public GameObject loadSlingShot(string model){
		// load model
		currentModel = Instantiate( Resources.Load ("SlingShots/" + model)) as GameObject;

		currentModel.GetComponent <Transform> ().parent = this.gameObject.transform;
		currentModel.GetComponent <Transform> ().localPosition = offset;

		currentModel.name = "slingshot";
		//currentModel.SetActive(true);
		// sync model info
		GetComponent<slingshotAPIs> ().updateModel ();
		GetComponent<slingshotStates> ().updateModel ();
		GetComponent<slingshotView> ().updateModel ();
		//return
		return currentModel;
	}
}
