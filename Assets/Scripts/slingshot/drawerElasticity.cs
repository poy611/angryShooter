﻿using UnityEngine;
using System.Collections;

public class drawerElasticity : MonoBehaviour {
	[Header("줄의 탄성입니다")]
	public float elasticity = 50.0f;
	[Header("발사가 되는 위치입니다.")]
	public float minimumShootingRange = 0.1f;
	[Header("여기까지 끌어야 발사가 됩니다")]
	public float minimumDrawingRange = 0.3f;
	[Header("여기부터 고무줄이 감속 됩니다")]
	public float stopPos = 0.07f;

	public float distance;
	[Header("현재 고무줄의 상대 위치입니다")]
	private float formerDist;
	[Header("Launcher에 shoot_power로 넘겨지는 값입니다")]
	public float releaseDist;
	[Header("화면상 끌기가 가능한 영역입니다")]
	public float min_y = 0.05f;
	public float max_y = 0.6f;

	private Vector3 origin;
	private GameObject originPos;
	private GameObject slingshot;
	private drawerDraggable drag;
	void Start(){
		originPos = GameObject.Find ("drawerOrigin");
		slingshot = GameObject.Find ("slingshot");
		drag = GetComponent<drawerDraggable> ();
	}
	void FixedUpdate (){
//		Vector3 originInWorld = Camera.main.ViewportToWorldPoint (new Vector3 (0.5f, 0.5f, transform.position.z));
//		Vector3 localpos = slingshot.transform.InverseTransformPoint (originInWorld);
//		origin = new Vector3 (transform.localPosition.x, localpos.y, transform.localPosition.z);
		origin = originPos.transform.localPosition;
//		Debug.DrawLine (localpos, origin);

		if (slingshot == null) {
			slingshot = GameObject.Find ("slingshot");
		}
		// 작동 조건.
		if (drag.dragging) {
			GetComponent<drawerLoader> ().autoLoaded = true;
		}
		releaseDist = Vector3.Distance (origin, transform.position);
		if (!drag.dragging && !drag.isOnShooting) {

			Vector3 direction = origin - transform.localPosition;
			//distance = Vector3.Distance (origin, transform.localPosition);
			distance = GetComponent<drawerDraggable> ().power_ratio;
			float power = GetComponent<drawerDraggable> ().shoot_power;
			// 발사 여부. <- 비율로 바꿀 것 using power_ratio사용할 것 위로당김을 고려할 것
			if (distance > minimumShootingRange) //안전거리 밖.
				GetComponent<Rigidbody> ().AddForce (direction * elasticity);
			else if (GetComponent<drawerLoader> ().currentBall != null && formerDist > minimumShootingRange && power > minimumDrawingRange) {
				drag.isOnShooting = true;
				GetComponent<drawerLauncher> ().launchBall (releaseDist);
			}
			// 멈춤 조건.
			if (distance < stopPos) {
				GetComponent<Rigidbody> ().velocity = GetComponent<Rigidbody> ().velocity / 2;

			}
			// power비율 계산

			// 드래그 한계
			Vector3 newViewPos = Camera.main.WorldToViewportPoint (transform.position);
			newViewPos.y = newViewPos.y < min_y ? min_y : newViewPos.y;
			newViewPos.y = newViewPos.y > max_y ? max_y : newViewPos.y;


			/*Vector3 localpos = slingshot.transform.InverseTransformPoint (
				Camera.main.ViewportToWorldPoint (newViewPos));*/
			GetComponent<Transform> ().localPosition = new Vector3(
				transform.localPosition.x, transform.localPosition.y, transform.localPosition.z);
			formerDist = distance;
		}
	}
}
