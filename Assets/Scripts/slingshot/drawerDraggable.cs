﻿using UnityEngine;
using System.Collections;

public class drawerDraggable : MonoBehaviour {
	public bool fix_x = false;
	public bool fix_y = false;
	public bool fix_z = false;
	public bool dragging = false;
	public bool isOnShooting = false;

	public float min_x = 0.05f;
	public float max_x = 0.95f;
	public float min_y = 0.05f;
	public float max_y = 0.95f;
	public float power_ratio = 0.0f;
	public float shoot_power = 0.0f;
	protected Vector3 mousePos;
	protected Vector3 currentScreenPos;
	protected Vector3 currentWorldPos;

	public GameObject originPos;
	public GameObject slingshot;

	void Start(){
		originPos = GameObject.Find ("drawerOrigin");
		slingshot = GameObject.Find ("slingshot");

	}

	void Update(){
		if (slingshot == null) {
			slingshot = GameObject.Find ("slingshot");
		}
		power_ratio = 1 - (Camera.main.WorldToViewportPoint (transform.position).y - min_y) / (Camera.main.WorldToViewportPoint (slingshot.transform.TransformPoint(originPos.transform.localPosition)).y - min_y);
	}

	IEnumerator OnMouseDown(){

		dragging = true;
		Vector3 scrSpace = Camera.main.WorldToScreenPoint (transform.position);
		float x, y, z;
		if (fix_x)	x = scrSpace.x;	else x = Input.mousePosition.x;	
		if (fix_y)	y = scrSpace.y;	else y = Input.mousePosition.y;
		if (fix_z)	z = scrSpace.z;	else z = Input.mousePosition.z;
		Vector3 offset = transform.position - Camera.main.ScreenToWorldPoint (new Vector3 (x,y,z));

		currentWorldPos = Camera.main.ScreenToWorldPoint (new Vector3(x,y,z)) + offset;
		GetComponent<Transform> ().position = currentWorldPos;


		while(Input.GetMouseButton(0)){
			

			mousePos = Input.mousePosition;

			if (fix_x)	x = scrSpace.x;	else x = mousePos.x;	
			if (fix_y)	y = scrSpace.y;	else y = mousePos.y;
			if (fix_z)	z = scrSpace.z;	else z = mousePos.z;
			currentScreenPos = new Vector3 (x,y,z);

			currentWorldPos = Camera.main.ScreenToWorldPoint (currentScreenPos) + offset;
			GetComponent<Transform> ().position = currentWorldPos;

			Vector3 newScreenPos = Camera.main.WorldToViewportPoint (GetComponent<Transform>().position);

			newScreenPos.x = newScreenPos.x < min_x ? min_x : newScreenPos.x;
			newScreenPos.x = newScreenPos.x > max_x ? max_x : newScreenPos.x;
			newScreenPos.y = newScreenPos.y < min_y ? min_y : newScreenPos.y;
			newScreenPos.y = newScreenPos.y > max_y ? max_y : newScreenPos.y;

			GetComponent<Transform>().position = Camera.main.ViewportToWorldPoint (newScreenPos);

			shoot_power = power_ratio;
			yield return null;

		
		}

		dragging = false;

	}
}
