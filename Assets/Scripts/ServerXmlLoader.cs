using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Xml;

public class ServerXmlLoader : MonoBehaviour
{
    public static ServerXmlLoader _Instance = null;

    public static ServerXmlLoader instance
    {
        get
        {
            if(_Instance == null)
            {
                _Instance = GameObject.FindObjectOfType(typeof(ServerXmlLoader)) as ServerXmlLoader;
                if (_Instance == null)
                    _Instance = new GameObject("ServerXmlLoader").AddComponent<ServerXmlLoader>();
            }
            Debug.Log("MakeSXL : " + _Instance);
            return _Instance;
        }
    }

    public void getConfXmlfromServer()
    {
        Debug.Log("start Load");
        WWW www = new WWW("http://itsnowball.com/masterconfig/cfg_2017_germwar.xml");

        while (!www.isDone) ;

        if (www.error != null)
        {
            Application.Quit();
            Debug.Log("xml is not found");
        }

        XmlDocument confXml = new XmlDocument();
        confXml.LoadXml(www.text.Trim());

        XmlNodeList temp = confXml.GetElementsByTagName("limitplaytime");
        float XplayTime = float.Parse(temp.Item(0).InnerText);

        StartCoroutine(xPlayTimer(XplayTime));
    }

    IEnumerator xPlayTimer(float XplayTime)
    {
		UnityEngine.SceneManagement.SceneManager.LoadScene (1);

        yield return new WaitForSeconds(XplayTime);
		Debug.Log("End Of time");
        Application.Quit();
    }
    private void Start()
    {
        DontDestroyOnLoad(this);
		getConfXmlfromServer ();
    }
}
