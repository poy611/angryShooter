﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundOnOff : MonoBehaviour {
	public GameObject enable;
	public GameObject disable;
	public void SoundOnOffOnClick(){
		SoundManage.instance.SetIsMute ();
	}
	void Update(){
		if (SoundManage.instance.GetIsMute ()) {
			enable.SetActive(false);
			disable.SetActive (true);
		} else {
			disable.SetActive (false);
			enable.SetActive(true);
		}
	}
}
