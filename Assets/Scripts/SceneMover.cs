﻿using UnityEngine;
using System.Collections;

public class SceneMover : MonoBehaviour {
	public string[] SceneLabels = {
		"test_scene", "1_Title_scene","2_Main_scene"
	};
	public void LoadSceneToScene(string name){
		SoundManage.instance.buttonClickPlay ();
		UnityEngine.SceneManagement.SceneManager.LoadScene ("Menu_"+name);
	}
	public void LoadSceneByName(){
		//
		SoundManage.instance.buttonClickPlay ();
		string name = "Menu_";
		string currnetName = UnityEngine.SceneManagement.SceneManager.GetActiveScene ().name;
		name += currnetName.Substring (0, 6);
		SoundManage.instance.BgmMainMenuPlay ();
		UnityEngine.SceneManagement.SceneManager.LoadScene (name);
	}
	public void LoadSceneByIndex(int idx){
		SoundManage.instance.buttonClickPlay ();
		UnityEngine.SceneManagement.SceneManager.LoadScene (SceneLabels [idx]);
	}
	public void Retry(){
		SoundManage.instance.buttonClickPlay ();
		UnityEngine.SceneManagement.SceneManager.LoadScene (UnityEngine.SceneManagement.SceneManager.GetActiveScene().name);
	}
	public void EndGame(){
		SoundManage.instance.buttonClickPlay ();
		Application.Quit ();
	}
	public void LastEnd(){
		SoundManage.instance.buttonClickPlay ();
		SoundManage.instance.BgmMainMenuPlay ();
		UnityEngine.SceneManagement.SceneManager.LoadScene ("Menu_Main");
	}
}
