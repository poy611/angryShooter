﻿using UnityEngine;
using System.Collections;

public class lineRenderer : MonoBehaviour {

	public GameObject lineStartObject;
	public string lineStartString;
	public Vector3 lineStartOffset;

	public GameObject lineEndObject;
	public string lineEndString;
	public Vector3 lineEndOffset;

	public float startWidth;
	public float endWidth;

	private LineRenderer liner;
	public Vector3 debugS;
	public Vector3 debugE;
	void Start () {
		liner = GetComponent<LineRenderer> ();
		if (lineStartObject == null)
			lineStartObject = GameObject.Find (lineStartString);
		
		if (lineEndObject == null)
			lineEndObject = GameObject.Find (lineEndString);
		liner.enabled = true;
	}

	void Update () {
		Vector3 posStart = lineStartObject.GetComponent<Transform> ().position;
		Vector3 posEnd = lineEndObject.GetComponent<Transform> ().position;
		debugS = posStart;
		debugE = posEnd;
	
		liner.SetPosition (0, new Vector3 (posStart.x + lineStartOffset.x, posStart.y + lineStartOffset.y, posStart.z + lineStartOffset.z));
		liner.SetPosition (1, new Vector3 (posEnd.x + lineEndOffset.x, posEnd.y + lineEndOffset.y, posEnd.z + lineEndOffset.z));
		liner.SetWidth (startWidth, endWidth);
	}

	public void changeLine(string textureName){
		Texture2D new_texture = (Texture2D)Resources.Load ("slingshot_lines/" + textureName);
		GetComponent<Renderer> ().material.mainTexture = new_texture;
	}
}
