﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShieldLifeTime : MonoBehaviour {
	
	// Use this for initialization
	void Awake () {
		StartCoroutine (ShiledLife ());
	}
	
	IEnumerator ShiledLife(){
		GameObject obj = transform.FindChild ("Shiled").gameObject;
		while (true) {
			yield return new WaitForSeconds (4.5f);
			obj.SetActive (false);
			yield return new WaitForSeconds (4.5f);
			obj.SetActive (true);

		}
	}
	void OnDestroy(){
		StopAllCoroutines ();
	}
}
