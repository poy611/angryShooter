﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class FireAni : MonoBehaviour {
	public Sprite[] sptires;
	public Image imgs;
	public float time = 1.0f;
	// Use this for initialization
	void Start () {
		StartCoroutine (Fire ());
	}

	IEnumerator Fire(){
		while (true) {
			yield return new WaitForSeconds (time);
			imgs.sprite = sptires [0];
			yield return new WaitForSeconds (time);
			imgs.sprite = sptires [1];
		}
	}

}
