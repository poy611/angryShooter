﻿using UnityEngine;
using System.Collections;

public class kingLifeCycle : blockLifeCycle {
	public float block_hp = 5;
	Renderer p;
	bool isBlink = false;
	bool isKingMove=false;
	bool isHitKing = false;
	string kingName;
	public Transform[] movePosition;
	public GameObject[] kingObj;

	public override void setup(){
		_blockHP = block_hp;
		kingName = transform.name;
		switch (kingName) {
		case "Monster(build)":
			blockType = BlockType.KING1;
			if (movePosition.Length==0) {
				movePosition = GameObject.Find ("Monster(build)Mp").GetComponentsInChildren<Transform> ();
			}
			break;
		case "Monster(plosion)":
			blockType = BlockType.KING2;
			if (movePosition.Length==0) {
				movePosition = GameObject.Find ("Monster(plosion)Mp").GetComponentsInChildren<Transform> ();
			}
			break;
		case "Monster(Sphere)":
			blockType = BlockType.KING3;
			if (movePosition.Length==0) {
				movePosition = GameObject.Find ("Monster(Sphere)Mp").GetComponentsInChildren<Transform> ();
			}
			break;
		case "Monster(flybuck)":
			blockType = BlockType.KING4;
			if (movePosition.Length==0) {
				movePosition = GameObject.Find ("Monster(flybuck)Mp").GetComponentsInChildren<Transform> ();
			}
			break;
		case "Witch":
			blockType = BlockType.LASTKING;
			break;
		}

		StartCoroutine (KingMove());

	}

	// update전에 수행
	public override void routine_event(){

	}
	// On Collision Enter수행 결정
	public override bool collide_event(Collision col){

		return false;
	}
	public override void SetTextureByHp (int hp)
	{
		
		if (blockType == BlockType.KING1) {
			if (hp < 50) {
				if (!isBlink) {
					p = GetComponentInChildren<Renderer> ();
					Texture before = Resources.Load ("Textures/monster_build", typeof(Texture)) as Texture;
					Texture after = Resources.Load ("Textures/monster_build_collapse", typeof(Texture)) as Texture;
					p.material.EnableKeyword ("_DETAIL_MULX2");
					StartCoroutine (BlinkKing(before,after));
				} else {
				}
			} 
		}
		if (blockType == BlockType.KING2) {
			if (hp < 50) {
				if (!isBlink) {
					p = GetComponent<Renderer> ();
					Texture before = Resources.Load ("Textures/monster_plosion", typeof(Texture)) as Texture;
					Texture after = Resources.Load ("Textures/monster_plosion_collapse", typeof(Texture)) as Texture;
					p.material.EnableKeyword ("_DETAIL_MULX2");
					StartCoroutine (BlinkKing(before,after));
				} else {
				}
			} 
		}
		if (blockType == BlockType.KING3) {
			if (hp < 50) {
				if (!isBlink) {
					p = GetComponentInChildren<Renderer> ();
					Texture before = Resources.Load ("Textures/monster_sphere", typeof(Texture)) as Texture;
					Texture after = Resources.Load ("Textures/monster_sphere_collapse", typeof(Texture)) as Texture;
					p.material.EnableKeyword ("_DETAIL_MULX2");
					StartCoroutine (BlinkKing(before,after));
				} else {
				}
			} 
		}
		if (blockType == BlockType.KING4) {
			if (hp < 50) {
				if (!isBlink) {
					p = GetComponentInChildren<Renderer> ();
					Texture before = Resources.Load ("Textures/monster_flybuck", typeof(Texture)) as Texture;
					Texture after = Resources.Load ("Textures/monster_flybuck_collapse", typeof(Texture)) as Texture;
					p.material.EnableKeyword ("_DETAIL_MULX2");
					StartCoroutine (BlinkKing(before,after));
				} else {
				}
			} 
		}
		if (blockType == BlockType.LASTKING) {
			if (hp < 50) {
				if (!isBlink) {
					p = GetComponentInChildren<Renderer> ();
					Texture before = Resources.Load ("Textures/witch", typeof(Texture)) as Texture;
					Texture after = Resources.Load ("Textures/witch_collapse", typeof(Texture)) as Texture;
					p.material.EnableKeyword ("_DETAIL_MULX2");
					StartCoroutine (BlinkKing(before,after));
				} else {
				}
			} 
		}
	}

	IEnumerator KingMove(){
		if (!isKingMove) {
			isKingMove = true;
			GameObject lookAtPositon = GameObject.Find ("ARCamera");
			GameObject ground = GameObject.Find ("GROUND");
			if(blockType == BlockType.LASTKING){
				int currentKingPostion = 0;
				int currentObjCount = 0;
				while(true){

					this.transform.FindChild ("Last").transform.FindChild ("Active_03").gameObject.SetActive (false);
					yield return new WaitForSeconds (2.0f);
				
					this.transform.FindChild ("Last").transform.FindChild ("Active_03").gameObject.SetActive (true);
					if (GetComponentInChildren<MeshRenderer>().enabled) {
						GameObject obj = Instantiate (kingObj [currentKingPostion]) as GameObject;
						obj.name = obj.name.Substring (0, obj.name.Length - 7);
						obj.transform.parent = this.transform;

						currentObjCount++;
						if (currentObjCount >= 4) {
							yield break;
						}
						currentKingPostion++;
						if (currentKingPostion == kingObj.Length - 1) {
							currentKingPostion = 0;
						}
					}
					yield return new WaitForSecondsRealtime (8.0f);
				}
			}
			else{
				int currentMovePostion = 0;
				while (true) {
					/*if(blockType == BlockType.KING3 || blockType == BlockType.KING4){
					transform.LookAt (lookAtPositon.transform.position);
					}*/

					float speed = 1.0f * Time.deltaTime;
					transform.position = Vector3.MoveTowards (transform.position, movePosition [currentMovePostion].position,speed);
					if (Vector3.Distance (movePosition[currentMovePostion].position,transform.position) <= 0.1f) {
						currentMovePostion++;
						if (currentMovePostion == movePosition.Length) {
							currentMovePostion = 0;
						}
					}
					yield return null;
				}
			}
		}
	}
	IEnumerator BlinkKing(Texture before, Texture after){
		if (!isBlink) {
			isBlink = true;
			while (true) {
				yield return new WaitForSecondsRealtime (1.0f);
				p.material.SetTexture ("_DetailAlbedoMap", after);
				yield return new WaitForSecondsRealtime (1.0f);
				p.material.SetTexture ("_DetailAlbedoMap", before);
			}
		}
		yield return null;
	}
	public override IEnumerator HitKing ()
	{
		Debug.Log ("HIT KING");
		if (!isHitKing) {
			isHitKing = true;
			p = GetComponentInChildren<Renderer> ();
			p.material.color = Color.red;
			yield return new WaitForSeconds (0.3f);
			p.material.color = Color.gray;
			yield return new WaitForSeconds (0.3f);
			p.material.color = Color.red;
			yield return new WaitForSeconds (0.3f);
			p.material.color = Color.gray;
			yield return new WaitForSeconds (0.3f);
			isHitKing = false;
		}
	}
}
