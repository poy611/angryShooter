﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class SetCharacterName : MonoBehaviour {

	public Sprite[] sprites;
	void OnEnable () {
		if (!PlayerPrefs.HasKey ("Character"))
			PlayerPrefs.SetString ("Character", "berry");
		string character = PlayerPrefs.GetString ("Character");
		switch (character) {
		case "berry":
			GetComponent<Image> ().sprite = sprites[0];
			break;
		case "kiki":
			GetComponent<Image> ().sprite = sprites[1];
			break;
		case "hoi":
			GetComponent<Image> ().sprite = sprites[2];
			break;
		case "pang":
			GetComponent<Image> ().sprite = sprites[3];
			break;
		}
	}
}
