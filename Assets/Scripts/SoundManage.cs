﻿using UnityEngine;
using System.Collections;


public class SoundManage : MonoBehaviour 
{
	public AudioSource bgmAudio;                   //Drag a reference to the audio source which will play the sound effects.
	public AudioSource buttonAudio;                 //Drag a reference to the audio source which will play the music.
	public GameObject effectObj;
	public static SoundManage instance = null;     //Allows other scripts to call functions from SoundManager.             
	public AudioClip _bgmTitle;
	public AudioClip _bgmMainMenu;
	public AudioClip _bgmGamePlay;
	public AudioClip _buttonClick;
	public AudioClip _jailCrash;
	public AudioClip _woodCrash;
	public AudioClip _stoneCrash;
	public AudioClip _fellowFly;
	public AudioClip _sling;
	public AudioClip _bombHit;
	public AudioClip _missileHit;
	public AudioClip _stoneHit;
	public AudioClip _stageClear;
	public AudioClip _stageFail;
	bool isMute = false;
	void Awake ()
	{
			//Check if there is already an instance of SoundManager
		if (instance == null)
				//if not, set it to this.
				instance = this;
		//If instance already exists:
		else if (instance != this)
				//Destroy this, this enforces our singleton pattern so there can only be one instance of SoundManager.
			Destroy (gameObject);

			//Set SoundManager to DontDestroyOnLoad so that it won't be destroyed when reloading our scene.
		DontDestroyOnLoad (gameObject);
	}
	public void SetIsMute(){
		if (isMute) {
			bgmAudio.enabled = true;
			buttonAudio.enabled = true;
			isMute = false;
		} else {
			bgmAudio.enabled = false;
			buttonAudio.enabled = false;
			isMute = true;
		}
	}
	public bool GetIsMute(){
		return isMute;
	}
	public void BgmTitlePlay(){
		bgmAudio.Stop ();
		bgmAudio.clip = _bgmTitle;
		bgmAudio.loop = true;
		if(!isMute)
			bgmAudio.Play ();
	}
	public void BgmMainMenuPlay(){
		bgmAudio.Stop ();
		bgmAudio.clip = _bgmMainMenu;
		bgmAudio.loop = true;
		if(!isMute)
			bgmAudio.Play ();
	}
	public void BgmGamePlayPlay(){
		bgmAudio.Stop ();
		bgmAudio.clip = _bgmGamePlay;
		bgmAudio.loop = true;
		if(!isMute)
			bgmAudio.Play ();
	}
	public void buttonClickPlay(){
		if(!isMute)
			buttonAudio.PlayOneShot (_buttonClick,1.0f);
	}
	public void JailCrashPlay(){
		if(!isMute)
			StartCoroutine (JailCrashPlayInBack ());
	}
	IEnumerator JailCrashPlayInBack(){
		AudioSource _audio = effectObj.AddComponent<AudioSource> () as AudioSource;
		_audio.PlayOneShot (_jailCrash,1.0f);
		while (true) {
			if (!_audio.isPlaying) {
				break;
			}
			yield return null;
		}
		Destroy (_audio);
	}
	public void WoodCrashPlay(){
		if(!isMute)
			StartCoroutine (WoodCrashPlayInBack ());
	}
	IEnumerator WoodCrashPlayInBack(){
		AudioSource _audio = effectObj.AddComponent<AudioSource> () as AudioSource;
		_audio.PlayOneShot (_woodCrash,1.0f);
		while (true) {
			if (!_audio.isPlaying) {
				break;
			}
			yield return null;
		}
		Destroy (_audio);
	}
	public void StoneCrashPlay(){
		if(!isMute)
			StartCoroutine (StoneCrashPlayInBack ());
	}
	IEnumerator StoneCrashPlayInBack(){
		AudioSource _audio = effectObj.AddComponent<AudioSource> () as AudioSource;
		_audio.PlayOneShot (_stoneCrash,1.0f);
		while (true) {
			if (!_audio.isPlaying) {
				break;
			}
			yield return null;
		}
		Destroy (_audio);
	}
	public void FellowFlyPlay(){
		if(!isMute)
			StartCoroutine (FellowFlyPlayInBack ());
	}
	IEnumerator FellowFlyPlayInBack(){
		AudioSource _audio = effectObj.AddComponent<AudioSource> () as AudioSource;
		_audio.PlayOneShot (_fellowFly,1.0f);
		while (true) {
			if (!_audio.isPlaying) {
				break;
			}
			yield return null;
		}
		Destroy (_audio);
	}
	public void SlingPlay(){
		if(!isMute)
			StartCoroutine (slingPlayInBack ());
	}
	IEnumerator slingPlayInBack(){
		AudioSource _audio = effectObj.AddComponent<AudioSource> () as AudioSource;
		_audio.PlayOneShot (_sling,1.0f);
		while (true) {
			if (!_audio.isPlaying) {
				break;
			}
			yield return null;
		}
		Destroy (_audio);
	}
	public void BombHitPlay(){
		if(!isMute)
			StartCoroutine (BombHitPlayInBack ());
	}
	IEnumerator BombHitPlayInBack(){
		AudioSource _audio = effectObj.AddComponent<AudioSource> () as AudioSource;
		_audio.PlayOneShot (_bombHit,1.0f);
		while (true) {
			if (!_audio.isPlaying) {
				break;
			}
			yield return null;
		}
		Destroy (_audio);
	}
	public void MissileHitPlay(){
		if(!isMute)
			StartCoroutine (MissileHitPlayInBack ());
	}
	IEnumerator MissileHitPlayInBack(){
		AudioSource _audio = effectObj.AddComponent<AudioSource> () as AudioSource;
		_audio.PlayOneShot (_missileHit,1.0f);
		while (true) {
			if (!_audio.isPlaying) {
				break;
			}
			yield return null;
		}
		Destroy (_audio);
	}
	public void StoneHitPlay(){
		if(!isMute)
			StartCoroutine (StoneHitPlayInBack ());
	}
	IEnumerator StoneHitPlayInBack(){
		AudioSource _audio = effectObj.AddComponent<AudioSource> () as AudioSource;
		_audio.PlayOneShot (_stoneHit,1.0f);
		while (true) {
			if (!_audio.isPlaying) {
				break;
			}
			yield return null;
		}
		Destroy (_audio);
	}
	public void StageClearPlay(){
		bgmAudio.Stop ();
		bgmAudio.clip = _stageClear;
		bgmAudio.loop = false;
		if(!isMute)
			bgmAudio.Play ();
	}

	public void StageFailPlay(){
		bgmAudio.Stop ();
		bgmAudio.clip = _stageFail;
		bgmAudio.loop = false;
		if(!isMute)
			bgmAudio.Play ();
	}
		
}


