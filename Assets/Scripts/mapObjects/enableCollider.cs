﻿using UnityEngine;
using System.Collections;

public class enableCollider : MonoBehaviour {
	
	void Update(){
		if (GetComponent<BoxCollider> () != null)
			GetComponent<BoxCollider> ().enabled = true;
		//if (GetComponent<SphereCollider> () != null)
		//	GetComponent<SphereCollider> ().enabled = true;
		if (GetComponent<CapsuleCollider> () != null)
			GetComponent<CapsuleCollider> ().enabled = true;
	}
}
