﻿using UnityEngine;
using System.Collections;
public enum HpLevel{
	hp100, hp75, hp50, hp25, hp0}
public class caseMgr : MonoBehaviour {
	public float endurance;
	public float block_hp;
	public GameObject ground;
	public float impulse;
	public GameObject _unopened;
	//public GameObject _opened;
	public GameObject _friend;
	[Header("새장이 사라지는 시간 입니다")]
	public float cageFadeSec = 3.0f;
	public bool damageCount = false;

	void Start(){
		endurance = block_hp;
	}
	void OnCollisionEnter(Collision col){
		if (damageCount) {
			impulse = GetComponent<Rigidbody> ().velocity.sqrMagnitude;
			endurance -= impulse;
			/* PlayLogger */
			if(GameObject.Find ("DebugLog") != null)
			GameObject.Find ("DebugLog").GetComponent<Logger> ().LogText (gameObject.name + ":lifecycle/OnCollisionEnter(col)/" + col.gameObject.name + "충돌");
			if (endurance < 0) {
				//GameObject smoketrail = Resources.Load ("Effect/Effect_07") as GameObject; // SmokeTrail_Cage
				//Instantiate (smoketrail, gameObject.transform.position, Quaternion.Euler (transform.forward));
				destroyCage ();
			}
		}
	}
	void Update(){
		if (ground == null && GameObject.Find ("ImageTarget") != null)
			ground = GameObject.Find ("ImageTarget");
		if (gameObject.transform.position.y < -2f)
			destroyCage ();
	}
	public void resetBlockHp(){
		endurance = block_hp;
		damageCount = true;

	}
	void destroyCage(){

		/* PlayLogger */
		if(GameObject.Find ("DebugLog") != null)
		GameObject.Find("DebugLog").GetComponent<Logger> ().LogText (gameObject.name + ":lifecycle/destroyCage()/케이스삭제");

		_unopened.SetActive (false);

		GameObject opened = 
			Instantiate ((GameObject)Resources.Load ("MapObjects/CaseOpened")) as GameObject;
		opened.transform.position = transform.position;
		opened.transform.parent = null;
		_friend.transform.parent = opened.transform;
		opened.GetComponent<caseOpened> ()._friend = _friend;
		opened.GetComponent<caseOpened> ().marker = ground;
		opened.GetComponent<caseOpened> ().StartAnimation ();
		SoundManage.instance.JailCrashPlay ();
		Destroy (this.gameObject);

	}
}
