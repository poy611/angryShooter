using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;

public class caseOpened : MonoBehaviour {
	public GameObject _friend;
	public GameObject marker;
	public float time;
	public float timeF;
	public Vector3 destination = new Vector3 (3f, 6f, 3f);

	public float currentheight;
	Vector3 localposition, position;
	// 원운동 좌표

	void Start(){
		if (marker != null) {
			Transform t = marker.GetComponent<Transform> ();
			_friend.transform.parent = marker.transform;
			_friend.transform.localScale = new Vector3 (0.3f, 0.3f, 0.3f);
		}
	}

	public void StartAnimation(){
		StartCoroutine (startAnimation());
	}
	IEnumerator startAnimation(){
		
		yield return new WaitForSeconds (0.5f);

		if (marker == null) {
			
			Destroy (this.gameObject);
		}
		else {
			SoundManage.instance.FellowFlyPlay ();
			time = 0;
			timeF = 0;
			while (_friend.transform.position.y < destination.y-1f) {
				//Debug.Log (_friend.name + " position : " + _friend.transform.position.y);

				time += Time.deltaTime;
				timeF += 1.2f * Time.deltaTime;
				//Debug.Log (_friend.transform.name +" TimeF : " + timeF);
				Vector3 lerpPoint = Vector3.Lerp (_friend.transform.position, destination, time / 20);

				Vector3 lerpDirection = lerpPoint - _friend.transform.position;
				if (lerpDirection.y < 0) {
					lerpDirection.y *= -1;
				}
				_friend.transform.Translate (lerpDirection);
				if (timeF > 3.0f) {
					
					Destroy (_friend);
				
					yield break;
				}
				yield return null;
			}

			Destroy (_friend);

		
		}
		Destroy (this.gameObject);
		yield break;
	}

}
