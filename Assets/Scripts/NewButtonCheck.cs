﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class NewButtonCheck : MonoBehaviour {


	void Start () {
		Debug.Log ("NEWBUTTON");
		if (!PlayerPrefs.HasKey ("NEWIMAGE")) {
			PlayerPrefs.SetInt ("NEWIMAGE",0);
		}
		StartCoroutine (NewButtonCo ());
	}
	

	IEnumerator NewButtonCo(){
		while (true) {
			if (PlayerPrefs.GetInt ("NEWIMAGE") == 0) {
				this.transform.GetComponent<Image> ().enabled = false;
				this.transform.GetComponent<switchUI> ().StopCoNewImage ();
			} else {
				this.transform.GetComponent<Image> ().enabled = true;
				this.transform.GetComponent<switchUI> ().NewImageEnable ();
			}
			yield return new WaitForSeconds (1.0f);
		}
	}
}
