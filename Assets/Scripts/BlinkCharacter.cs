﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class BlinkCharacter : MonoBehaviour {
	public GameObject Blink;
	public float speed = 0.5f;
	void OnEnable(){
		StartCoroutine (BlackBlink ());
	}

	IEnumerator BlackBlink()
	{
		Color color = Blink.GetComponent<Image> ().color;
		//blink time
		while (true) {
			while (true) {

				yield return null;
				color.a -= Time.deltaTime*speed;
				Blink.GetComponent<Image> ().color = color; 
				if (color.a <= 0.5) {
					break;
				}
			}


			while (true) {
				yield return null;
				color.a += Time.deltaTime*speed;
				Blink.GetComponent<Image> ().color = color;

				if (color.a >= 1) {
					break;
				}
			}
			yield return new WaitForSeconds (0.5f);
		}

	}
	void OnDisable(){
		StopAllCoroutines ();
	}
}
