﻿using UnityEngine;
using System.Collections;

public class BlockLinkage : MonoBehaviour {
	public float impulse;
	public float endurance;
	public Vector3 savedVelocity;
	public Vector3 savedTorque;
	public GameObject[] dependencies;
	public bool impacted = false;
	void Update(){
		if (GameObject.Find ("ImageTarget") == null) {
			savedVelocity = GetComponent<Rigidbody> ().velocity;
			savedTorque = GetComponent<Rigidbody> ().angularVelocity;
			GetComponent<Rigidbody> ().isKinematic = true;
		} else {
			GetComponent<Rigidbody> ().isKinematic = false;
			GetComponent<Rigidbody> ().AddForce(savedVelocity, ForceMode.VelocityChange);
			GetComponent<Rigidbody> ().AddTorque (savedTorque, ForceMode.VelocityChange);
		}
		for (int i = 0; i < dependencies.Length; i++) {
			if (!dependencies [i].GetComponent<BlockLinkage> ().impacted)
				breakLink ();
		}
	}
	void OnCollisionEnter(Collision col){
		Debug.Log (col);
		Debug.Log (col);
		impulse = (col.impulse / Time.fixedDeltaTime).sqrMagnitude;

		if (impulse > endurance)
			breakLink();
	}
	void breakLink(){
		impacted = true;
		GetComponent<Rigidbody> ().angularDrag = 0.05f;
		GetComponent<Rigidbody> ().drag = 0.0f;
	}
}
