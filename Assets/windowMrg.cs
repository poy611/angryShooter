﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class windowMrg : MonoBehaviour {
	public bool windowEnabled = false;
	void Start(){
		windowEnabled = true;
	}
	public void enableWindow(){
		
		this.gameObject.SetActive (true);
	}
	public void disableWindow(){
		SoundManage.instance.BgmMainMenuPlay ();
		this.gameObject.SetActive (false);
	}
	public void toggleEnableWindow(){
		if (windowEnabled) {
			disableWindow ();
			windowEnabled = false;
		} else {
			enableWindow ();
			windowEnabled = true;
		}
	}

}
