﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
public class resultButton : MonoBehaviour {
	public SceneMover mover;
	public string sceneName;
	public Text scoreText;
	int currentScore;
	public int nowThemeNum;
	GetBlockType _getBlockType;
	int stars;
	public void OnResultButton(){
		if (GetComponent<Result> ().isClear) {
			stars = 0;
			sceneName = UnityEngine.SceneManagement.SceneManager.GetActiveScene ().name;
	
			_getBlockType = GameObject.Find ("ARCamera").GetComponent<GetBlockType> ();
			PlayerPrefs.SetInt (sceneName, 3);
			PlayerPrefs.SetInt ("currentStage", PlayerPrefs.GetInt ("currentStage") + 1);


			scoreText = transform.FindChild ("result").transform.FindChild ("image(score)").transform.FindChild ("Point").GetComponent<Text> ();
			currentScore = GetComponent<Result> ().totalScore;

			if (currentScore > PlayerPrefs.GetInt (sceneName + "_Star")) {
				if (PlayerPrefs.GetInt ("TotalScore") - PlayerPrefs.GetInt (sceneName + "_Star") < 0) {
					PlayerPrefs.SetInt ("TotalScore", 0);
				} else {
					PlayerPrefs.SetInt ("TotalScore", PlayerPrefs.GetInt ("TotalScore") - PlayerPrefs.GetInt (sceneName + "_Star"));
				}
				PlayerPrefs.SetInt ("TotalScore", PlayerPrefs.GetInt ("TotalScore") + currentScore);

			}
			PlayerPrefs.SetInt (sceneName + "_Star", PlayerPrefs.GetInt (sceneName + "_Star") < currentScore ? currentScore : PlayerPrefs.GetInt (sceneName + "_Star"));
			float totalCalculatedScore = GetComponent<Result> ().totalCalculatedScore;
			if (totalCalculatedScore >= 0.8f) {
				stars = 3;
			} else if (totalCalculatedScore >= 0.5f) {
				stars = 2;
			} else {
				stars = 1;
			}
			PlayerPrefs.SetInt (sceneName + "_StarCount", PlayerPrefs.GetInt (sceneName + "_StarCount") < stars ? stars : PlayerPrefs.GetInt (sceneName + "_StarCount"));
			SoundManage.instance.BgmMainMenuPlay ();
			PlayerPrefs.SetInt ("CharacterEffect", 1);
			if (sceneName == "Menu_ThemeLast") {
				mover.LastEnd ();
			} else if (sceneName.Substring (sceneName.Length - 2, 2) == "10") {
				if (!PlayerPrefs.HasKey ("NextScene" + (nowThemeNum + 1) + "isDone")) {
					PlayerPrefs.SetInt ("NextScene" + (nowThemeNum + 1), 1);
					PlayerPrefs.SetInt ("NextScene" + (nowThemeNum + 1) + "isDone", 1);
				} else {
					PlayerPrefs.SetInt ("NextScene" + (nowThemeNum + 1), 0);
				}
				PlayerPrefs.SetInt ("CharacterEffect", 0);
				mover.LoadSceneToScene ("Main");
			} else {
				mover.LoadSceneByName ();
			}
		} else {
			SoundManage.instance.BgmMainMenuPlay ();
			if (sceneName == "Menu_ThemeLast") {
				mover.LastEnd ();
			}  else {
				mover.LoadSceneByName ();
			}
		}
	}
}
