﻿using UnityEngine;
using System.Collections;
using System;

public class slingshotStates : MonoBehaviour {
	public enum launchState{
		unloaded, loaded, dragged, launched
	}
	[Header("공의 상태를 보여줍니다")]
	public launchState state;
	public GameObject drawer;
	public GameObject ball;
	[Header("공의 이름은 공발사시 새로운 공을 만들때 사용됩니다.")]
	public string ball_name;

	void Update(){
		ball_name = drawer.GetComponent<drawerLoader> ().ball_name;
		ball = GameObject.Find (ball_name);
		if (ball == null) {
			state = launchState.unloaded;
			drawer.GetComponent<drawerLoader>().autoLoaded = false;
			drawer.GetComponent<drawerDraggable> ().isOnShooting = false;
		}
		else{
			state = launchState.launched;
			if (ball.transform.parent != null) {
				if (ball.transform.parent.gameObject == drawer) {
					if (drawer.GetComponent<drawerDraggable> ().dragging) {
						state = launchState.dragged;
					} else {
						state = launchState.loaded;
					}
				}
			}
		}
	}
	public void updateModel(){
		drawer = GameObject.Find ("drawer");
	}
}
