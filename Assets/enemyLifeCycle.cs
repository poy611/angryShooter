﻿using UnityEngine;
using System.Collections;

public class enemyLifeCycle : MonoBehaviour {
	public float impulse;
	public float endurance;

	void OnCollisionEnter(Collision col){
		Debug.Log (col);
		impulse = (col.impulse / Time.fixedDeltaTime).sqrMagnitude;
		if (impulse > endurance) {
			Destroy (this.gameObject);
		}
	}
}
