﻿using UnityEngine;
using System.Collections;

public class strawLifeCycle : blockLifeCycle {
	public float block_hp = 2;
	public bool hasBall = false;
	public bool isBomb = false;
	public bool ballReady = false;
	public bool isDone = false;
	public string boomPath = "Effect/Detonator-Chunks";
	public string ballname;
	public GameObject ball;

	private float bombPower;
	private float curSize;

	// start 전에 수행
	public override void setup(){
		blockType = BlockType.STRAW;
		_blockHP = block_hp;
	}

	// update전에 수행
	public override void routine_event(){
		// 공 흡수한 후, 다음 공 장전
	}
	IEnumerator checkBallReady(){
		while (!ballReady) {
			if (slingshotstate.state == slingshotStates.launchState.loaded)
				ballReady = true;
			yield return null;
		}
	}
	// On Collision Enter수행 결정
	public override bool collide_event(Collision col){
		if (hasBall == false) {
			if (col.gameObject.tag == "ball") {
				hasBall = true;

				/* 텍스쳐 변환*/
				ballname = slingshotstate.ball_name;
				switch (slingshotstate.ball_name) {
				case "bomb":
					isBomb = true;
					bombPower = col.gameObject.GetComponent<ballBomb> ().bombPower;
					curSize = col.gameObject.GetComponent<ballBomb> ().curSize;
				// 텍스쳐 변환
					break;
				case "stone":
				// 텍스쳐 변환
					break;
				case "strait":
				// 텍스쳐 변환
					break;
				}
				this.gameObject.tag = "block";
				StartCoroutine (checkBallReady ());
				Destroy (col.gameObject);
			}
		}

		// 공 흡수한 후
		if(ballReady) {
			// 폭탄이 터지거나
			if (isBomb) {
				if (isDone == false) {
					GameObject explosion = Instantiate (Resources.Load ("Effect/Detonator-Chunks")) as GameObject;
					explosion.GetComponent<Transform> ().position = GetComponent<Transform> ().position;
					GetComponent<BoxCollider> ().isTrigger = true;
					SphereCollider myCollider = transform.GetComponent<SphereCollider> ();
					myCollider.isTrigger = false;
					curSize = myCollider.radius;
					myCollider.radius = curSize * bombPower;
					isDone = true;
					Destroy (this.gameObject);
				}
				return true;
			// 일반 블록처럼 대미지받음
			} else { 
				return false;
			}
		}
		return true;
	}
	public override void SetTextureByHp (int hp)
	{
		
	}
}