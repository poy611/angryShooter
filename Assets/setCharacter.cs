﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class setCharacter : MonoBehaviour {
	public Sprite[] icons;
	void Start () {
		if (!PlayerPrefs.HasKey ("Character"))
			PlayerPrefs.SetString ("Character", "berry");
		string character = PlayerPrefs.GetString ("Character");
		switch (character) {
		case "berry":
			GetComponent<Image> ().sprite = icons [0];
			break;
		case "kiki":
			GetComponent<Image> ().sprite = icons [1];
			break;
		case "hoi":
			GetComponent<Image> ().sprite = icons [2];
			break;
		case "pang":
			GetComponent<Image> ().sprite = icons [3];
			break;
		}
	}

	public void updateCharacter(string c){
		switch (c) {
		case "berry":
			GetComponent<Image> ().sprite = icons [0];
			break;
		case "kiki":
			GetComponent<Image> ().sprite = icons [1];
			break;
		case "hoi":
			GetComponent<Image> ().sprite = icons [2];
			break;
		case "pang":
			GetComponent<Image> ().sprite = icons [3];
			break;
		}
		PlayerPrefs.SetString ("Character", c);
	}
}
