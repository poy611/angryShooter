﻿using UnityEngine;
using System.Collections;

public class CharacterSelect : MonoBehaviour {

	public GameObject selectedEffect11;
	public GameObject selectedEffect12;
	public GameObject selectedEffect21;
	public GameObject selectedEffect22;
	public GameObject selectedEffect31;
	public GameObject selectedEffect32;
	public GameObject selectedEffect41;
	public GameObject selectedEffect42;
	public setCharacter setter;

	public string character;
	void OnEnable(){
		PlayerPrefs.SetInt ("NEWIMAGE", 0);
		checkCharacter ();
		switch (character) {
		case "berry":
			OnClickCharacter (1);
			break;
		case "kiki":
			OnClickCharacter (2);
			break;
		case "hoi":
			OnClickCharacter (3);
			break;
		case "pang":
			OnClickCharacter (4);
			break;
		default:
			OnClickCharacter (1);
			break;
		}
	}
	public void checkCharacter(){
		if(! PlayerPrefs.HasKey ("Character"))
			PlayerPrefs.SetString ("Character", "berry");
		character = PlayerPrefs.GetString ("Character");
	}
	public void confirmCharacter(){
		setter.updateCharacter (character);
		PlayerPrefs.SetString ("Character", character);
	}
	public void OnClickCharacter(int charNum){
		SoundManage.instance.buttonClickPlay ();
		selectedEffect11.SetActive (false);
		selectedEffect12.SetActive (false);
		selectedEffect21.SetActive (false);
		selectedEffect22.SetActive (false);
		selectedEffect31.SetActive (false);
		selectedEffect32.SetActive (false);
		selectedEffect41.SetActive (false);
		selectedEffect42.SetActive (false);
		switch (charNum) {
		case 1:
			selectedEffect11.SetActive (true);
			selectedEffect12.SetActive (true);
			character = "berry";
			break;
		case 2:
			selectedEffect21.SetActive (true);
			selectedEffect22.SetActive (true);
			character = "kiki";
			break;
		case 3:
			selectedEffect31.SetActive (true);
			selectedEffect32.SetActive (true);
			character = "hoi";
			break;
		case 4:
			selectedEffect41.SetActive (true);
			selectedEffect42.SetActive (true);
			character = "pang";
			break;
		}
	}

}
