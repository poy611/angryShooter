﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class BallSelectManager : MonoBehaviour {

	public slingshotAPIs api;
	public GameObject ballStrait;
	public GameObject ballBomb;
	public GameObject ballDefault;
	public GameObject iconStrait;
	public GameObject iconBomb;
	public GameObject iconDefault;
	public string currentBallName;
	gameplay _gameplay;
	public slingshotStates state;
	void Start(){
		_gameplay = GameObject.Find ("System").GetComponent<gameplay> ();
	}
	public void SetInitBallLoad(){
		if (_gameplay == null) {
			_gameplay = GameObject.Find ("System").GetComponent<gameplay> ();
		}
		ballDefault.GetComponent<Button>().interactable=false;
		if (_gameplay.currentBombCount > 0) {
			ballBomb.GetComponent<Button> ().interactable = true;
		} else {
			ballBomb.GetComponent<Button> ().interactable = false;
		}
		if (_gameplay.currentStraitCount > 0) {
			ballStrait.GetComponent<Button> ().interactable = true;
		} else {
			ballStrait.GetComponent<Button> ().interactable = false;
		}

		iconStrait.SetActive (false);
		iconBomb.SetActive (false);
		iconDefault.SetActive (true);
		currentBallName = "stone";


	}
	public void OnClickButton(string name){
		

			SoundManage.instance.buttonClickPlay ();
			//_gameplay.BallSelect (currentBallName, name);
			currentBallName = name;
			switch (name) {
			case "bomb":
				api.loadBall ("bomb");
			//ballBomb.SetActive (false);
				ballBomb.GetComponent<Button> ().interactable = false;
			//ballStrait.SetActive (true);
				if (_gameplay.currentStraitCount > 0)
					ballStrait.GetComponent<Button> ().interactable = true;
			//ballDefault.SetActive (true);
				if (_gameplay.currentStoneCount > 0)
					ballDefault.GetComponent<Button> ().interactable = true;

				iconStrait.SetActive (false);
				iconBomb.SetActive (true);
				iconDefault.SetActive (false);

				break;
			case "strait":
				api.loadBall ("strait");
			//ballStrait.SetActive (false);
			//ballBomb.SetActive (true);
			//ballDefault.SetActive (true);
				ballStrait.GetComponent<Button> ().interactable = false;
				if (_gameplay.currentBombCount > 0)
					ballBomb.GetComponent<Button> ().interactable = true;
				if (_gameplay.currentStoneCount > 0)
					ballDefault.GetComponent<Button> ().interactable = true;

				iconStrait.SetActive (true);
				iconBomb.SetActive (false);
				iconDefault.SetActive (false);
				break;
			case "stone":
				api.loadBall ("stone");
			//ballDefault.SetActive (false);
			//ballBomb.SetActive (true);
			//ballStrait.SetActive (true);
				ballDefault.GetComponent<Button> ().interactable = false;
				if (_gameplay.currentBombCount > 0)
					ballBomb.GetComponent<Button> ().interactable = true;
				if (_gameplay.currentStraitCount > 0)
					ballStrait.GetComponent<Button> ().interactable = true;

				iconStrait.SetActive (false);
				iconBomb.SetActive (false);
				iconDefault.SetActive (true);
				break;
			case "ball_selected":
				break;
			}

	}

	public void BallEmpty(string name){
		switch (name) {
		case "bomb":
			if (_gameplay.currentStoneCount > 0) {
				ballDefault.GetComponent<Button>().interactable=true;
				ballBomb.GetComponent<Button>().interactable=false;
				if(_gameplay.currentStraitCount>0)
					ballStrait.GetComponent<Button>().interactable=true;
				else
					ballStrait.GetComponent<Button>().interactable=false;
				iconStrait.SetActive (false);
				iconBomb.SetActive (false);
				iconDefault.SetActive (true);
				currentBallName = "stone";

			} else if (_gameplay.currentStraitCount > 0) {
				ballStrait.GetComponent<Button>().interactable=true;
				ballBomb.GetComponent<Button>().interactable=false;
				if (_gameplay.currentStoneCount > 0)
					ballDefault.GetComponent<Button> ().interactable = true;
				else
					ballDefault.GetComponent<Button> ().interactable = false;
				iconStrait.SetActive (true);
				iconBomb.SetActive (false);
				iconDefault.SetActive (false);
				currentBallName = "strait";

			}
			break;
		case "strait":
			if (_gameplay.currentStoneCount > 0) {
				ballDefault.GetComponent<Button>().interactable=true;
				ballStrait.GetComponent<Button> ().interactable = false;
				if(_gameplay.currentBombCount > 0)
					ballBomb.GetComponent<Button>().interactable=true;
				else
					ballBomb.GetComponent<Button>().interactable=false;
				iconStrait.SetActive (false);
				iconBomb.SetActive (false);
				iconDefault.SetActive (true);
				currentBallName = "stone";
			
			} else if (_gameplay.currentBombCount > 0) {
				ballBomb.GetComponent<Button> ().interactable = true;
				ballStrait.GetComponent<Button> ().interactable = false;
				if(_gameplay.currentStoneCount > 0)
					ballDefault.GetComponent<Button> ().interactable = true;
				else
					ballDefault.GetComponent<Button> ().interactable = false;
				iconStrait.SetActive (false);
				iconBomb.SetActive (true);
				iconDefault.SetActive (false);
				currentBallName = "bomb";
			
			}
			break;
		case "stone":
			if (_gameplay.currentBombCount > 0) {
				ballBomb.GetComponent<Button> ().interactable = true;
				ballDefault.GetComponent<Button> ().interactable = false;
				if(_gameplay.currentStraitCount>0)
					ballStrait.GetComponent<Button> ().interactable = true;
				else 
					ballStrait.GetComponent<Button> ().interactable = false;
				iconStrait.SetActive (false);
				iconBomb.SetActive (true);
				iconDefault.SetActive (false);
				currentBallName = "bomb";
			
			} else if (_gameplay.currentStraitCount > 0) {
				
				ballStrait.GetComponent<Button> ().interactable = true;
				ballBomb.GetComponent<Button> ().interactable = false;
				if(_gameplay.currentStoneCount > 0)
					ballDefault.GetComponent<Button> ().interactable = true;
				else
					ballDefault.GetComponent<Button> ().interactable = false;
				iconStrait.SetActive (true);
				iconBomb.SetActive (false);
				iconDefault.SetActive (false);
				currentBallName = "strait";
			
			}
			break;
		}
	}
}
