﻿using UnityEngine;
using System.Collections;
using System.Net;

public class drawerLoader : MonoBehaviour {
	public GameObject currentBall;
	public string ball_name;
	public float posOffset = 0.5f;
	public bool deleteball;
	public bool loadball;
	public bool autoLoaded = false;
	gameplay _gameplay;
	void Start(){
		_gameplay = GameObject.Find ("System").GetComponent<gameplay> ();
		ball_name = "stone";

	}
	void Update(){
		if (autoLoaded == false)
			StartCoroutine (loadBallAuto (ball_name));
		autoLoaded = true;
	}
	IEnumerator loadBallAuto(string ball_name){
		yield return new WaitForSeconds (1);
		if (currentBall == null)
			loadBall (ball_name);
	}
	public GameObject loadBall(string model){
		/* PlayLogger */
		switch (model) {
		case "bomb":
			if (_gameplay.currentBombCount <= 0) {
				//_gameplay.BallSelect (model);
				if (_gameplay.currentStoneCount > 0) {
					model = "stone";
				} else if (_gameplay.currentStraitCount > 0) {
					model = "strait";
				}
			}
			break;
		case "strait":
			if (_gameplay.currentStraitCount <= 0) {
				//_gameplay.BallSelect (model);
				if (_gameplay.currentStoneCount > 0) {
					model = "stone";
				} else if (_gameplay.currentBombCount > 0) {
					model = "bomb";
				}
			}
			break;
		case "stone":
			if (_gameplay.currentStoneCount <= 0) {
				//_gameplay.BallSelect (model);
				if (_gameplay.currentBombCount > 0) {
					model = "bomb";
				} else if (_gameplay.currentStraitCount > 0) {
					model = "strait";
				}
			}
			break;
		}
		if(GameObject.Find ("DebugLog") != null)
			GameObject.Find("DebugLog").GetComponent<Logger> ().LogText ("drawerLoader/loadBall(string)/장전");

		if (GetComponent<drawerLauncher> ().flyingBall != null)
			Destroy (GetComponent<drawerLauncher> ().flyingBall);
		if (currentBall != null)
			unloadBall ();
		ball_name = model;
		currentBall = Instantiate (Resources.Load ("Balls/Ball(" + ball_name + ")"), transform.position, transform.rotation) as GameObject;
		currentBall.GetComponent<Transform> ().parent = this.gameObject.transform;
		currentBall.GetComponent<Transform> ().localPosition = new Vector3 (0, 0, posOffset);
		if (ball_name == "strait") {
			currentBall.GetComponent<Transform> ().localRotation = new Quaternion (-10f, 0, 0, 90f);
		}
		currentBall.GetComponent<SphereCollider> ().isTrigger = true;
		currentBall.GetComponent<Rigidbody> ().useGravity = false;
		currentBall.name = ball_name;

		//_gameplay.BallSelect (model);
		_gameplay.EndOfGame();
		return currentBall;
	}
	public string unloadBall(){
		deleteball = true;
		if (currentBall == null)
			return null;
		string name = currentBall.name;
		Destroy (currentBall.gameObject);
		return name;
	}
}
