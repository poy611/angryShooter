﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class toggledebugger : MonoBehaviour {
	public GameObject debugwindow;
	public bool isTrue = true;

	public void Ontoggle(){
		if (isTrue) {
			debugwindow.GetComponent<Canvas> ().planeDistance = 0;
			isTrue = false;
		} else {
			debugwindow.GetComponent<Canvas> ().planeDistance = 1;
			isTrue = true;			
		}

	}
}
