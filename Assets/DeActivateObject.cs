﻿using UnityEngine;
using System.Collections;

public class DeActivateObject : MonoBehaviour {

	public GameObject obj;
	public void deactivate(){
		SoundManage.instance.buttonClickPlay ();
		obj.SetActive (false);
	}
}
