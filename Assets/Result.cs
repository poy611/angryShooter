﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;
public class Result : MonoBehaviour {
	public ImageEffectAccumulativeChange starControl;
	//public ImageEffectSuccessiveChange messageControl;
	public setCharacterGesture setter;
	public int score=0;
	public int stars = 1;
	public int[] grading_scores = {0,0,0};
	public Text _scoreText;
	resultButton _resultButton;
	GetBlockType _getBlockType;
	public GameObject scoreObj;
	public Sprite[] images;
	public Image messageImage;
	public int currentScore=0;
	gameplay _gamePlay;
	public float totalCalculatedScore = 0;
	public int leftWaponScore = 0;
	public int totalScore=0;

	public bool isClear = false;
	public void resultWindow(){
		isClear = true;
		SoundManage.instance.StageClearPlay ();
		_getBlockType = GameObject.Find ("ARCamera").GetComponent<GetBlockType> ();

		score = int.Parse (scoreObj.GetComponent<Text> ().text);
		stars = calculateStar ();
		starControl.SetImageWithIndex (stars);

		messageImage.sprite = images [stars - 1];
		setter.setCharacter (stars);
		_scoreText.text = "" + totalScore;
	}
	public void resultFailWindow(){
		isClear = false;
		SoundManage.instance.StageClearPlay ();

		_getBlockType = GameObject.Find ("ARCamera").GetComponent<GetBlockType> ();

		score = int.Parse (scoreObj.GetComponent<Text> ().text);

		stars = calculateStar ();

		starControl.SetImageWithIndex (0);
	
		messageImage.sprite = images [0];

		setter.setCharacter (1);

		_scoreText.text = "Fail";

	}
	public int calculateStar(){
		
		_gamePlay = GameObject.Find ("System").GetComponent<gameplay> ();

		leftWaponScore += _gamePlay.currentStoneCount * 100 * 5;
		leftWaponScore += _gamePlay.currentBombCount * 100 * 5;
		leftWaponScore += _gamePlay.currentStraitCount * 100 * 5;

		totalCalculatedScore = Min (1.0f, ((float)score / (float)_getBlockType.sceneTotalLimitScore * 0.7f + (((float)leftWaponScore / (float)_getBlockType.waponTotalScore) * 0.7f)));
		Debug.Log ("totalCalculatedScore + " + totalCalculatedScore);
		totalScore = score + leftWaponScore;

		if (totalCalculatedScore>=0.8f) {
			stars = 3;
		} else if (totalCalculatedScore>=0.5f) {
			stars = 2;
		} 
		else {
			stars = 1;
		}

		return stars;
	}
	void OnEnable(){
		GameObject.Find ("objs").gameObject.SetActive(false);
	}
	float Min(float a, float b){
		if (a >= b) {
			return b;
		} else {
			return a;
		}
	}
}
