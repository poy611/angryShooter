﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class gameplay : MonoBehaviour {

	public GameObject wrapper;
	[Header("SlowMode 여부입니다")]
	public bool SlowModeOn = false;
	[Header("슬로우 모드의 속도 입니다")]
	public float slowSpeed = 0.3f;
	[Header("점수 창입니다")]
	public GameObject calc;
	public GameObject closeview;
	public GameObject caution;
	public Text stoneCount;
	public Text bombCount;
	public Text straitCount;
	public int currentStoneCount;
	public int currentBombCount;
	public int currentStraitCount;
	BallSelectManager _ballSelecter;
	int[,] ballCountInfo = new int[,] { 
		//stone , bomb , strait
		{ 4, 0, 0 }, //stage1
		{ 4, 0, 0 }, //stage2
		{ 3, 2, 2 }, //stage3
		{ 4, 0, 0 }, //stage4
		{ 3, 0, 0 }, //stage5
		{ 4, 0, 0 }, //stage6
		{ 3, 2, 2 }, //stage7
		{ 4, 0, 0 }, //stage8
		{ 3, 0, 0 }, //stage9
		{ 3, 2, 2 }, //stage10

		{ 4, 0, 0 }, //stage1
		{ 4, 0, 0 }, //stage2
		{ 3, 2, 2 }, //stage3
		{ 4, 0, 0 }, //stage4
		{ 3, 0, 0 }, //stage5
		{ 4, 0, 0 }, //stage6
		{ 3, 2, 2 }, //stage7
		{ 4, 0, 0 }, //stage8
		{ 3, 0, 0 }, //stage9
		{ 3, 2, 2 }, //stage10
	

		{ 4, 0, 0 }, //stage1
		{ 4, 0, 0 }, //stage2
		{ 3, 2, 2 }, //stage3
		{ 4, 0, 0 }, //stage4
		{ 3, 0, 0 }, //stage5
		{ 4, 0, 0 }, //stage6
		{ 3, 2, 2 }, //stage7
		{ 4, 0, 0 }, //stage8
		{ 3, 0, 0 }, //stage9
		{ 3, 2, 2 }, //stage10

		{ 4, 0, 0 }, //stage1
		{ 4, 0, 0 }, //stage2
		{ 3, 2, 2 }, //stage3
		{ 4, 0, 0 }, //stage4
		{ 3, 0, 0 }, //stage5
		{ 4, 0, 0 }, //stage6
		{ 3, 2, 2 }, //stage7
		{ 4, 0, 0 }, //stage8
		{ 3, 0, 0 }, //stage9
		{ 3, 2, 2 }, //stage10

		{ 3, 2, 2 }, //stage41


	};
	/* PlayLogger */
	public bool logged = false;
	bool isEnd = false;
	void Start(){
		SoundManage.instance.BgmGamePlayPlay ();
		wrapper = GameObject.Find ("slingshotWrapper");
		_ballSelecter = GameObject.Find ("icons(ballShootMode)").GetComponent<BallSelectManager>();
		SetInitBallCount ();
	}

	public void EndOfGame(){
		
		if (!isEnd) {
			
			if ((GameObject.FindWithTag ("cage") == null && GameObject.FindWithTag ("_friend") == null)) {
				/* PlayLogger */
				if (!logged) {
					isEnd = true;
					if (GameObject.Find ("DebugLog") != null)
						GameObject.Find ("DebugLog").GetComponent<Logger> ().LogText 
					(gameObject.name + ":gameplay/Update()/케이스 없음, 게임종료");
					logged = true;
					closeview.SetActive (false);
					caution.GetComponent<CautionWindowControl> ().onPlay = false;

					wrapper.SetActive (false);
					calc.SetActive (true);
				
					calc.GetComponent<Result> ().resultWindow ();

				}
			} else if ((currentStoneCount <= 0 && currentBombCount <= 0 && currentStraitCount <= 0)) {
				if (!logged) {
					isEnd = true;
					if (GameObject.Find ("DebugLog") != null)
						GameObject.Find ("DebugLog").GetComponent<Logger> ().LogText 
					(gameObject.name + ":gameplay/Update()/케이스 없음, 게임종료");
					logged = true;
					closeview.SetActive (false);
					caution.GetComponent<CautionWindowControl> ().onPlay = false;

					wrapper.SetActive (false);
					calc.SetActive (true);

					calc.GetComponent<Result> ().resultFailWindow ();

				}
			}
		}
	}
	void slowmode(bool isSlow){
		if (wrapper.GetComponent<slingshotStates> ().state == slingshotStates.launchState.launched) {
			if (Time.timeScale == 1.0F) {
				Time.timeScale = slowSpeed;
				Time.fixedDeltaTime = 0.001f * Time.timeScale;
			}
		} else if (Time.timeScale == slowSpeed){
			Time.timeScale = 1.0F;
			Time.fixedDeltaTime = 0.02f * Time.timeScale;
		}
	}
	int getStageIndex(){
		return UnityEngine.SceneManagement.SceneManager.GetActiveScene ().buildIndex-6;
	}
	void SetInitBallCount(){
		currentStoneCount = ballCountInfo [getStageIndex(),0];
		currentBombCount = ballCountInfo [getStageIndex(),1];
		currentStraitCount = ballCountInfo [getStageIndex(),2];
		stoneCount.text = "" + ballCountInfo [getStageIndex(),0];
		bombCount.text = "" + ballCountInfo [getStageIndex(),1];
		straitCount.text = "" + ballCountInfo [getStageIndex(),2];
		_ballSelecter.SetInitBallLoad ();
		//currentStoneCount -= 1;
		//stoneCount.text = "" + currentStoneCount;
	}

	public void LaunchBall(string ballName){
		//SoundManage.instance.SlingsSoundPlay ();
		switch (ballName) {
		case "stone":
			//currentStoneCount -= 1;
			if (currentStoneCount < 0) {
				_ballSelecter.BallEmpty ("stone");
			} else {
				stoneCount.text = "" + currentStoneCount;
			}
			break;
		case "bomb":
			//currentBombCount -= 1;
			if (currentBombCount < 0) {
				_ballSelecter.BallEmpty ("bomb");
			} else {
				bombCount.text = "" + currentBombCount;
			}
			break;
		case "strait":
			//currentStraitCount -= 1;
			if (currentStraitCount < 0) {
				_ballSelecter.BallEmpty ("strait");
			} else {
				straitCount.text = "" + currentStraitCount;
			}
			break;
		}
	}
	public void BallSelect(string prevBallName , string nextBallName){
		if (prevBallName == "stone") {
			currentStoneCount += 1;
			stoneCount.text = "" + currentStoneCount;
		} else if (prevBallName == "bomb") {
			currentBombCount += 1;
			bombCount.text = "" + currentBombCount;
		} else {
			currentStraitCount += 1;
			straitCount.text = "" + currentStraitCount;
		}

/*		if (nextBallName == "stone") {
			currentStoneCount -= 1;
			stoneCount.text = "" + currentStoneCount;
		} else if (nextBallName == "bomb") {
			currentBombCount -= 1;
			bombCount.text = "" + currentBombCount;
		} else {
			currentStraitCount -= 1;
			straitCount.text = "" + currentStraitCount;
		}*/
	}

	public void BallSelect(string ballName){
		
		switch (ballName) {
		case "stone":
			currentStoneCount -= 1;
			if (currentStoneCount <= 0) {
				_ballSelecter.BallEmpty ("stone");
				stoneCount.text = "0";
			} else {
				stoneCount.text = "" + currentStoneCount;
			}

			break;
		case "bomb":
			currentBombCount -= 1;
			if (currentBombCount <= 0) {
				_ballSelecter.BallEmpty ("bomb");
				bombCount.text = "0";
			} else {
				bombCount.text = "" + currentBombCount;
			}
			break;
		case "strait":
			currentStraitCount -= 1;
			if (currentStraitCount <= 0) {
				_ballSelecter.BallEmpty ("strait");
				straitCount.text = "0";
			} else {
				straitCount.text = "" + currentStraitCount;
			}
			break;
		}

	}
}
