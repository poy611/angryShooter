﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ImageEffectHorizontalFlowing : MonoBehaviour {

	public GameObject image1;
	public GameObject image2;
	public float interval = 3f;
	
	private float height;
	private float width;
	private bool animation_on = false;

	void Start() {
		
		height = image1.GetComponent<RectTransform> ().rect.height;
		//width = 3831f;//image1.GetComponent<RectTransform> ().rect.width;
		width = image1.GetComponent<RectTransform> ().rect.width;
	}
	void Update () {
		if (!animation_on) {
			animation_on = true;
			StartCoroutine (flowingAnimation ());
		}
	}

	IEnumerator flowingAnimation(){
		Debug.Log ("FlowingAnimation");
		float time = 0;
		float curh;
		while (time < interval) {
		//while(image1.GetComponent<RectTransform> ().anchoredPosition.x>=-3831f){
			
			time += Time.fixedDeltaTime;
			curh = (time / interval * width);
			image1.GetComponent<RectTransform> ().offsetMin = new Vector2 (-curh, 0);
			image1.GetComponent<RectTransform> ().SetSizeWithCurrentAnchors (RectTransform.Axis.Horizontal, width);
			image1.GetComponent<RectTransform> ().SetSizeWithCurrentAnchors (RectTransform.Axis.Vertical, height);
			image2.GetComponent<RectTransform> ().offsetMin = new Vector2 (width - curh-0.3f, 0);
			image2.GetComponent<RectTransform> ().SetSizeWithCurrentAnchors (RectTransform.Axis.Horizontal, width);
			image2.GetComponent<RectTransform> ().SetSizeWithCurrentAnchors (RectTransform.Axis.Vertical, height);
			yield return null;
		}
		//image1.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (0f, 0f);
		//image2.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (5751f, 0f);

		animation_on = false;
	}
}
