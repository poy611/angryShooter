﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class messagesUI : MonoBehaviour {

	public Camera cam;
	public Canvas canvas;
	public float w;
	public float h;
	public GameObject[] cages;
	public GameObject[] helps;
	public GameObject[] thankyous;
	public Vector3[] poses;

	void Start(){
		cages = GameObject.FindGameObjectsWithTag ("cage");
		helps = new GameObject[cages.Length];
		poses = new Vector3[cages.Length];
		for (int i=0; i<helps.Length; i++) {
			GameObject help = Resources.Load ("Effect/balloon_help_image") as GameObject;
			Vector2 cage_pos = cam.WorldToScreenPoint (cages [i].GetComponent<Transform> ().position);
			helps[i] = Instantiate (help, cage_pos, Quaternion.identity) as GameObject;
			helps[i].transform.SetParent (this.transform, false);
		}
	}
	void Update () {
		// projection
		for(int i=0; i<cages.Length; i++){
			if (cages[i] != null) {
				Vector3 cagepos = cam.WorldToViewportPoint (cages [i].transform.position);
				Vector3 helppos = cam.ViewportToScreenPoint (cagepos);
				poses [i] = new Vector3 (cagepos.x * 1920, (cagepos.y + 0.05f)* 1080, 0);
				
				helps [i].GetComponent<RectTransform> ().anchoredPosition = poses [i];
			} else{
				helps [i].SetActive (false);
			}
		}
	}
}
