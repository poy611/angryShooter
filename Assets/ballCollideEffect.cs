﻿using UnityEngine;
using System.Collections;

public class ballCollideEffect : MonoBehaviour {

	public GameObject collideObject;
	public Vector3 collideRotation;
	//public AudioClip collideSound;

	bool gameStarted = false;
	[SerializeField]
	bool collideWithGround = false;

	void OnCollisionStay(Collision col){
		if (col.gameObject.name == "GROUND")
			collideWithGround = true;
		else
			collideWithGround = false;
	}

	void OnCollisionEnter(Collision col){
		collideObject = col.gameObject;

		switch (this.transform.name) {
		case "stone":
			SoundManage.instance.StoneHitPlay ();
			break;
		case "bomb":
			SoundManage.instance.BombHitPlay ();
			break;
		case "strait":
			SoundManage.instance.MissileHitPlay ();
			break;
		}
		if (gameStarted) {
			if (collideWithGround) {
				foreach (ContactPoint contact in col.contacts) {
					GameObject smoketrail = Resources.Load ("Effect/SmokeTrail_Block") as GameObject;
					Instantiate (smoketrail, contact.point, Quaternion.Euler (contact.normal.x, contact.point.y, contact.point.z));
					Debug.DrawRay (contact.point, contact.normal, Color.white);
				}
			}
		}
	}
}
