﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class ThemeMgr : MonoBehaviour {

	public int clearedPlusOne;
	public int currentTheme;
	public int currentStage;
	/*
	public GameObject clear1;
	public GameObject clear2;
	public GameObject clear3;
	public GameObject clear4;
	*/

	public void getState(){
		
	
		if (!PlayerPrefs.HasKey ("currentStage"))
			PlayerPrefs.SetInt ("currentStage", 1);
		clearedPlusOne = PlayerPrefs.GetInt ("currentStage");
		if (!PlayerPrefs.HasKey ("TotalScore")) {
			PlayerPrefs.SetInt ("TotalScore", 0);
		}

		GameObject.Find ("TotalScore").GetComponent<Text> ().text = PlayerPrefs.GetInt ("TotalScore")+"";
		for (int i = 1; i <= 40; i++) {
			int theme = (i-1) / 10 + 1;
			int stage = (i-1) % 10+1;
			string stageName = "Theme" + theme.ToString () + "_Scene" + stage.ToString ();
			Debug.Log (stageName + " : " + PlayerPrefs.GetInt (stageName).ToString());
			if (PlayerPrefs.GetInt (stageName) > 0) {
				
				switch (i / 10 + 1) {
				case 2:
					if (PlayerPrefs.GetInt ("NextScene2") != 0) {
						GameObject.Find ("Theme2").GetComponent<switchUI> ().enableUIEffect ();
						//PlayerPrefs.SetInt ("NEWIMAGE",1);
					} else {
						GameObject.Find ("Theme1").transform.FindChild ("Enabled").transform.FindChild ("Line").gameObject.SetActive (true);
						GameObject.Find ("Theme2").GetComponent<switchUI> ().enableUI ();
					}
					break;
				case 3:
					if (PlayerPrefs.GetInt ("NextScene3") != 0) {
						GameObject.Find ("Theme3").GetComponent<switchUI> ().enableUIEffect ();
						PlayerPrefs.SetInt ("NEWIMAGE",1);
					} else {
						GameObject.Find ("Theme2").transform.FindChild ("Enabled").transform.FindChild ("Line").gameObject.SetActive (true);
						GameObject.Find ("Theme3").GetComponent<switchUI> ().enableUI ();
					}
					break;
				case 4:
					if (PlayerPrefs.GetInt ("NextScene4") != 0) {
						GameObject.Find ("Theme4").GetComponent<switchUI> ().enableUIEffect ();
						PlayerPrefs.SetInt ("NEWIMAGE",1);
					} else {
						GameObject.Find ("Theme3").transform.FindChild ("Enabled").transform.FindChild ("Line").gameObject.SetActive (true);
						GameObject.Find ("Theme4").GetComponent<switchUI> ().enableUI ();
					}
					break;
				case 5:
					if (PlayerPrefs.GetInt ("NextScene5") != 0) {
						GameObject.Find ("Theme5").GetComponent<switchUI> ().enableUIEffect ();
						PlayerPrefs.SetInt ("NEWIMAGE",1);
					} else {
						GameObject.Find ("Theme4").transform.FindChild ("Enabled").transform.FindChild ("Line").gameObject.SetActive (true);
						GameObject.Find ("Theme5").GetComponent<switchUI> ().enableUI ();
					}
					break;
				}
				clearedPlusOne = i+1;
			} else {
				/* 
				if(PlayerPrefs.GetInt (stageName) < 0){
					PlayerPrefs.SetInt (stageName, 0);
					if (i % 10 + 1 == 1) {
						switch (i / 10 + 1) {
						case 2:
							GameObject.Find ("ClearMessageTheme1").SetActive (true);
							break;
						case 3:
							clear2.SetActive (true);
							break;
						case 4:
							clear3.SetActive (true);
							break;
						case 5:
							clear4.SetActive (true);
							break;
						}
					} 
				}*/
				/*if (i % 10 + 1 == 1) {
					switch (i / 10 + 1) {
					case 2:
						if (PlayerPrefs.GetInt ("NextScene2") != 0) {
							GameObject.Find ("Theme2").GetComponent<switchUI> ().enableUIEffect ();
						} else {
							GameObject.Find ("Theme1").transform.FindChild ("Enabled").transform.FindChild ("Line").gameObject.SetActive (true);
							GameObject.Find ("Theme2").GetComponent<switchUI> ().enableUI ();
						}
						break;
					case 3:
						if (PlayerPrefs.GetInt ("NextScene3") != 0) {
							GameObject.Find ("Theme3").GetComponent<switchUI> ().enableUIEffect ();
						} else {
							GameObject.Find ("Theme2").transform.FindChild ("Enabled").transform.FindChild ("Line").gameObject.SetActive (true);
							GameObject.Find ("Theme3").GetComponent<switchUI> ().enableUI ();
						}
						break;
					case 4:
						if (PlayerPrefs.GetInt ("NextScene4") != 0) {
							GameObject.Find ("Theme4").GetComponent<switchUI> ().enableUIEffect ();
						} else {
							GameObject.Find ("Theme3").transform.FindChild ("Enabled").transform.FindChild ("Line").gameObject.SetActive (true);
							GameObject.Find ("Theme4").GetComponent<switchUI> ().enableUI ();
						}
						break;
					case 5:
						if (PlayerPrefs.GetInt ("NextScene5") != 0) {
							GameObject.Find ("Theme5").GetComponent<switchUI> ().enableUIEffect ();
						} else {
							GameObject.Find ("Theme4").transform.FindChild ("Enabled").transform.FindChild ("Line").gameObject.SetActive (true);
							GameObject.Find ("Theme5").GetComponent<switchUI> ().enableUI ();
						}
						break;
					}
				}*/
				break;
			}
		}
		currentTheme = (clearedPlusOne - 1) / 10 + 1;
		currentStage = (clearedPlusOne - 1) % 10 + 1;
	}
	void Start(){
		//PlayerPrefs.DeleteAll ();
		getState ();

	}

}
