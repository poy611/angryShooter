﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class helpMessageBlink : MonoBehaviour {
	/* 1. 110% 2. 100%수렴 3. 2초간유지 후 펑터짐 5. 7초 있다가 반복 */
	private float t1 = 0.4f;
	private float t2 = 0.1f;
	private float t3 = 2f;
	private float t5 = 7f;
	public bool increasing = false;
	private float time;
	private float scale;
	// Update is called once per frame
	void Update () {
		if (increasing == false) {
			time += Time.deltaTime;
			if (time / (t5) > 0) {
				increasing = true;
				time = 0;
			}
		} else {
			time += Time.deltaTime;
			if (time < t1 + t2) {
				if (time < t1)
					scale = time / t1 * 1.01f;
				else
					scale = 1.01f - (time - t1) / t2 * 0.1f;
				GetComponent<RectTransform> ().localScale = new Vector3 (scale, scale, scale);
				GetComponent<Image> ().color = new Color (255, 255, 255, (time / t1 + t2));
			} else if (time > t1 + t2 + t3) {
				GetComponent<RectTransform> ().localScale = new Vector3 (0,0,0);
				increasing = false;
			}
	}}
}
