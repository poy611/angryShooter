﻿using UnityEngine;
using System.Collections;

public class ActivateObject : MonoBehaviour {

	public GameObject obj;
	public void activate(){
		SoundManage.instance.buttonClickPlay ();
		obj.SetActive (true);
	}
}
