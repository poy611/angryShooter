﻿using UnityEngine;
using System.Collections;

public class ImageEffectAccumulativeChange : MonoBehaviour {
	public GameObject[] images;
	public float interval;
	public bool smooth;
	public bool auto = false;

	private int index = 0;
	private int cur = 0;
	private bool animationOn = false;

	public void SetImageWithIndex(int n){
		StartCoroutine (SetImageWithIndexCoru(n));
	}
	IEnumerator SetImageWithIndexCoru(int n){
		yield return new WaitForSecondsRealtime (0.3f);
		int i = 0;
		while(i<n){
			images [i].SetActive (true);

			i++;
			yield return new WaitForSecondsRealtime (0.3f);
		}
	}
	// Use this for initialization
	void Start () {
		index = images.Length;
	}
	
	// Update is called once per frame
	void Update () {
		if (auto) {
			if (!animationOn) {
				animationOn = true;
				StartCoroutine (changingAnimation ());
			}
		} else {
			animationOn = false;
		}
	}

	IEnumerator changingAnimation(){
		cur = 0;
		while (cur < index) {
			images [cur].SetActive (true);
			float time = 0;
			while (time < interval) {
				time += Time.deltaTime;
				yield return null;
			}
			cur++;
		}
		animationOn = false;
		while (--cur > 0)
			images [cur].SetActive (false);
	}
}
