﻿using UnityEngine;
using System.Collections;
 
public class CameraFacingBillboard : MonoBehaviour
{
	public Camera m_Camera;
	public Vector3 initialScale;
	public float initialDistance;
	public float factor;
	void Start(){
		if (m_Camera == null)
			m_Camera = Camera.main;
		initialScale = GetComponent<Transform> ().localScale;
		initialDistance = (GetComponent<Transform> ().position - 
			m_Camera.transform.position).sqrMagnitude;
	}
	void Update()
	{
		transform.LookAt(transform.position + m_Camera.transform.rotation * Vector3.forward,
			m_Camera.transform.rotation * Vector3.up);
		float dist = (GetComponent<Transform> ().position - 
			m_Camera.transform.position).sqrMagnitude;
		GetComponent<Transform> ().localScale = initialScale * Mathf.Sqrt(dist / initialDistance);
	}
}