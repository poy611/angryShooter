﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class blockLifeCycle : MonoBehaviour {

	public float _blockHP;
	public float impulse;
	public float _endurance;
	public bool damageCount = false;

	public GameObject collideObj;
	public slingshotStates slingshotstate;

	public HpLevel hplevel = HpLevel.hp100;
	public int hp;
	public GameObject ppoint;
	int point_int;
	public int direction_offset = -1;
	public GameObject balm;
	public enum BlockType{
		IRON,
		STONE,
		WOOD,
		STRAW,
		KING1,
		KING2,
		KING3,
		KING4,
		LASTKING
	};
	// start 전에 수행
	public BlockType blockType;
	public virtual void setup(){}
	public virtual void routine_event(){}
	public virtual bool collide_event(Collision col){return false;}
	public virtual void SetTextureByHp (int hp){}
	public virtual IEnumerator HitKing(){
		yield return new WaitForSeconds (0.1f);
	}
	void Start(){
		setup ();
		_endurance = _blockHP;
		Debug.Log ("endurance : " + _endurance);
		slingshotstate = GameObject.Find ("slingshotWrapper").GetComponent<slingshotStates> ();
		if (ppoint == null)
			ppoint = GameObject.FindWithTag ("point");
		
	}

	void Update(){
		routine_event ();
		if (transform.position.y < -5)
			Destroy (transform.gameObject);
		hp = (int)(_endurance / _blockHP * 100);

		int damage = 0;

	    if (hp < 0) {
			if (hplevel != HpLevel.hp0) {
				damage = HpLevel.hp0 - hplevel;
				hplevel = HpLevel.hp0;

				SetTextureByHp (hp);
			}
		}
		else if (hp < 25) {
			if (hplevel != HpLevel.hp25) {
				damage = HpLevel.hp25 - hplevel;
				hplevel = HpLevel.hp25;
				SetTextureByHp (hp);
			}
		} else if (hp < 50) {
			if (hplevel != HpLevel.hp50) {
				damage = HpLevel.hp50 - hplevel;
				hplevel = HpLevel.hp50;
				SetTextureByHp (hp);
			}
		} else if (hp < 75) {
			if (hplevel != HpLevel.hp75) {
				damage = HpLevel.hp75 - hplevel;
				hplevel = HpLevel.hp75;
				SetTextureByHp (hp);
			}
		}
		else if(hp<100){
			if (hplevel != HpLevel.hp100) {
				damage = HpLevel.hp100 - hplevel;
				hplevel = HpLevel.hp100;
				SetTextureByHp (hp);
			}
		}
		if (ppoint == null)
			ppoint = GameObject.FindWithTag ("point");
		else {
			string point_text = ppoint.GetComponent<Text> ().text;
			point_int = int.Parse (point_text);
			GameObject message;
			if (damage > 0) {
				message = Instantiate (Resources.Load ("Effect/ScoreMessage"), transform.position, transform.rotation) as GameObject;
				//GameObject _obj = GameObject.Find("Messages");
				//message.transform.SetParent(_obj.transform, false);
				if (direction_offset == 2)
					direction_offset = -1;
				switch (damage) {
				case 1:
					message.GetComponent<ScoreMessageUI> ().score = 25;
					/*큐빅 방법 - easy-in*/
					message.GetComponent<ScoreMessageUI> ().offset = direction_offset;
					direction_offset = (direction_offset + 1) % 3;
					point_int += 25;
					ppoint.GetComponent<Text> ().text = point_int.ToString ();

					break;
				case 2:
					message.GetComponent<ScoreMessageUI> ().score = 50;
					message.GetComponent<ScoreMessageUI> ().offset = direction_offset;
					direction_offset = (direction_offset + 1) % 3;
					point_int += 50;
					ppoint.GetComponent<Text> ().text = point_int.ToString ();
					break;
				case 3:
					message.GetComponent<ScoreMessageUI> ().score = 75;
					message.GetComponent<ScoreMessageUI> ().offset = direction_offset;
					direction_offset = (direction_offset + 1) % 3;
					point_int += 75;
					ppoint.GetComponent<Text> ().text = point_int.ToString ();
					break;
				case 4:
					message.GetComponent<ScoreMessageUI> ().score = 100;
					message.GetComponent<ScoreMessageUI> ().offset = direction_offset;
					direction_offset = (direction_offset + 1) % 3;
					point_int += 100;
					ppoint.GetComponent<Text> ().text = point_int.ToString ();

					break;
				default:
					break;
				}
			}
		}

	}
	public void resetBlockHp(){
		_endurance = _blockHP;
		damageCount = true;
	}
	void OnCollisionEnter(Collision col){
		
		collideObj = col.gameObject;
		if (slingshotstate == null)
			slingshotstate = GameObject.Find ("slingshotWrapper").GetComponent<slingshotStates> ();

		if (collide_event (col) == false)
			
			if (damageCount) {
				Debug.Log ("Collider Enter");
				impulse = GetComponent<Rigidbody> ().velocity.sqrMagnitude;
				_endurance -= impulse;
				/* PlayLogger */
				if (blockType == BlockType.KING1 || blockType == BlockType.KING2 || blockType == BlockType.KING3 || blockType == BlockType.KING4 || blockType == BlockType.LASTKING) {
				StartCoroutine (HitKing());
				}
				if (GameObject.Find ("DebugLog") != null) {
						GameObject.Find ("DebugLog").GetComponent<Logger> ().LogText (gameObject.name + ":lifecycle/OnCollisionExit/\nimpulse : " + impulse + "\nendurance : " + _endurance + "");
						//Debug.Log (gameObject.name + ":lifecycle/OnCollisionExit/\nimpulse : " + impulse + "\nendurance : " + _endurance + "");
				}

				if (_endurance < 0) {
					
					StartCoroutine (destroyBlock ());
				}
					//destroyBlock ();
			}
	}

	IEnumerator destroyBlock(){
		yield return new WaitForSeconds (0.3f);
		/* PlayLogger */
		if (GameObject.Find ("DebugLog") != null)
		if (blockType == BlockType.STONE) {
			SoundManage.instance.StoneCrashPlay ();
		} else if (blockType == BlockType.WOOD) {
			SoundManage.instance.WoodCrashPlay ();
		}
		GameObject.Find("DebugLog").GetComponent<Logger> ().LogText (gameObject.name + ":lifecycle/destroyball/\n블록제거");
		GameObject boomBalm = Instantiate (balm, this.transform.position, Quaternion.identity) as GameObject;
		Destroy (this.gameObject);
	}

}
